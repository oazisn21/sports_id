package com.pt2.sportid.Mahasiswa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.pt2.sportid.Model.Dosen.DosenModel;
import com.pt2.sportid.Model.EditProfil.EditProfilModel;
import com.pt2.sportid.Model.Jurusan.Data;
import com.pt2.sportid.Model.Jurusan.JurusanModel;
import com.pt2.sportid.Model.Kelas.KelasModel;
import com.pt2.sportid.Model.Profile.ProfileModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;
import com.pt2.sportid.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahProfil extends AppCompatActivity {

    private ApiService mApiService;
    ImageView ivFotoProfil;
    EditText etNim, etNama, etUsername, etPasswordOld, etPasswordNew, etConfirmPasswordNew;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPref;;
    RadioButton rbMale, rbFemale;
    Spinner spDosen, spJurusan,spKelas;
    Button btnSimpan;
    String url,fotoPath, jk;
    private ImagePicker imagePicker;
    private CameraImagePicker cameraImagePicker;
    private String id_jurusan;
    com.pt2.sportid.Model.Dosen.Data[] dosen;
    com.pt2.sportid.Model.Kelas.Data[] kelas;
    com.pt2.sportid.Model.Jurusan.Data[] jurusan;
    private String id_kelas;
    String nama_kelas, nama_dosen, nama_jurusan;
    ArrayAdapter<String> dataAdapterJurusan, dataAdapterKelas, dataAdapterDosen;
    private String id_dosen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_profil_mahasiswa);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //initComponents
        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);
        ivFotoProfil = (ImageView) findViewById(R.id.iv_upload);
        etNim = (EditText) findViewById(R.id.et_nim);
        etNama = (EditText) findViewById(R.id.et_nama);
        etUsername = (EditText) findViewById(R.id.et_username);
        etPasswordOld = (EditText) findViewById(R.id.et_passwordOld);
        etPasswordNew = (EditText) findViewById(R.id.et_password);
        etConfirmPasswordNew = (EditText) findViewById(R.id.et_password_confirm);
        rbMale = (RadioButton) findViewById(R.id.rb_l);
        rbFemale = (RadioButton) findViewById(R.id.rb_p);
        spDosen = (Spinner) findViewById(R.id.s_dosen);
        spJurusan = (Spinner) findViewById(R.id.s_jurusan);
        spKelas = (Spinner) findViewById(R.id.s_kelas);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        imagePicker = new ImagePicker(this);
        cameraImagePicker = new CameraImagePicker(this);

        //shared preferences
        sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        //setOnclickListener
        rbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jk = "L";
            }
        });
        rbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jk = "P";
            }
        });
        ivFotoProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] items = {"Ambil Foto", "Pilih dari penyimpanan",
                        "Batalkan"};
                AlertDialog.Builder builder = new AlertDialog.Builder(UbahProfil.this);
                builder.setTitle("Tambah Foto Diri");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Ambil Foto")) {
                            fotoPath = cameraImagePicker.pickImage();
                        } else if (items[item].equals("Pilih dari penyimpanan")) {
                            imagePicker.pickImage();
                        } else if (items[item].equals("Batalkan")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
        imagePicker.setImagePickerCallback(new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> list) {
                Glide.with(getApplicationContext())
                        .load(Uri.fromFile(new File(list.get(0).getOriginalPath())))
                        .into(ivFotoProfil);
                fotoPath = list.get(0).getOriginalPath();
                Log.wtf("fotoPath", fotoPath);
            }

            @Override
            public void onError(String s) {
                Toast.makeText(getApplicationContext(), "error : " + s, Toast.LENGTH_SHORT).show();
            }
        });
        cameraImagePicker.setImagePickerCallback(new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> list) {
                Glide.with(getApplicationContext())
                        .load(fotoPath)
                        .into(ivFotoProfil);
                Log.wtf("fotoPath", fotoPath);
            }

            @Override
            public void onError(String s) {
                Toast.makeText(getApplicationContext(), "error : " + s, Toast.LENGTH_SHORT).show();
            }
        });
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simpan();
            }
        });

        progressDialog.show();
        showData();
        setSpinnerJurusan();
        setSpinnerDosen();
    }

    private void simpan() {
        final String nim = etNim.getText().toString();
        final String nama = etNama.getText().toString();
        final String username = etUsername.getText().toString();
        String password = etPasswordOld.getText().toString();
        final String passwordNew = etPasswordNew.getText().toString();
        String passwordConfirmationNew = etConfirmPasswordNew.getText().toString();
        if (TextUtils.isEmpty(id_kelas) ||
                TextUtils.isEmpty(nama) ||
                TextUtils.isEmpty(nim) ||
                TextUtils.isEmpty(id_dosen) ||
                TextUtils.isEmpty(id_kelas) ||
                TextUtils.isEmpty(id_jurusan) ||
                TextUtils.isEmpty(username) ||
                TextUtils.isEmpty(jk)){
            Log.wtf("meh","meeeh");
            Toast.makeText(getApplicationContext(), "Mohon lengkapi kolom yang ada", Toast.LENGTH_SHORT).show();
        }
        else if (!TextUtils.isEmpty(password) ) {
            if (TextUtils.isEmpty(passwordNew) || TextUtils.isEmpty(passwordConfirmationNew)) {
                Toast.makeText(getApplicationContext(), "Mohon lengkapi kolom yang ada!", Toast.LENGTH_SHORT).show();
            }
        }
        else if(!passwordNew.equals(passwordConfirmationNew)){
            Toast.makeText(getApplicationContext(), "Konfirmasi kata sandi tidak cocok!", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(fotoPath)){
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Konfirmasi");
            builder.setMessage("Apakah anda yakin dengan data ini?");
            builder.setPositiveButton("Ya",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressDialog.show();

                            mApiService.editProfilMahasiswa(
                                    generate(sharedPref.getString(getString(R.string.token_pref),"0")),
                                    generate("2"),
                                    generate(username),
                                    generate(passwordNew),
                                    generate(nim),
                                    generate(nama),
                                    generate(jk),
                                    null,
                                    generate(id_jurusan),
                                    generate(id_dosen),
                                    generate(id_kelas)
                            ).enqueue(new Callback<EditProfilModel>() {
                                @Override
                                public void onResponse(Call<EditProfilModel> call, Response<EditProfilModel> response) {
                                    if(response.isSuccessful()){
                                        Toast.makeText(getApplicationContext(),"Data berasil berubah",Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                    else{
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(),"Data gagal berubah",Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<EditProfilModel> call, Throwable t) {
                                    if (t instanceof NoConnectivityException) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Gagal merubah data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                                        Log.wtf("sportsid", t.getMessage());
                                        finish();
                                    } else {
                                        progressDialog.dismiss();
                                        Log.wtf("sportsid", t.getMessage());
                                        Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            });
                        }
                    });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();



        }
        else{
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Konfirmasi");
            builder.setMessage("Apakah anda yakin dengan data ini?");
            builder.setPositiveButton("Ya",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressDialog.show();

                            File file = new File(fotoPath);

                            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                            MultipartBody.Part image = MultipartBody.Part.createFormData("foto", file.getName(), reqFile);

                            mApiService.editProfilMahasiswa(
                                    generate(sharedPref.getString(getString(R.string.token_pref),"0")),
                                    generate("2"),
                                    generate(username),
                                    generate(passwordNew),
                                    generate(nim),
                                    generate(nama),
                                    generate(jk),
                                    image,
                                    generate(id_jurusan),
                                    generate(id_dosen),
                                    generate(id_kelas)
                            ).enqueue(new Callback<EditProfilModel>() {
                                @Override
                                public void onResponse(Call<EditProfilModel> call, Response<EditProfilModel> response) {
                                    if(response.isSuccessful()){
                                        Toast.makeText(getApplicationContext(),"Data berasil berubah",Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                    else{
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(),"Data gagal berubah",Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<EditProfilModel> call, Throwable t) {
                                    if (t instanceof NoConnectivityException) {
                                        progressDialog.hide();
                                        Toast.makeText(getApplicationContext(), "Gagal merubah data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                                        Log.wtf("sportsid", t.getMessage());
                                        finish();
                                    } else {
                                        progressDialog.hide();
                                        Log.wtf("sportsid", t.getMessage());
                                        Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            });
                        }
                    });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    RequestBody generate(String text) {
        return RequestBody.create(MediaType.parse("text/plain"), text);
    }

    private void showData() {
        mApiService.getMyProfile(sharedPref.getString(getString(R.string.token_pref),"0")).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                if(response.isSuccessful()){
                    Glide.with(getApplicationContext())
                            .load(response.body().getData().getFoto())
                            .into(ivFotoProfil);
                    url = response.body().getData().getFoto();
                    etNama.setText(response.body().getData().getNama());
                    etUsername.setText(response.body().getData().getUser().getUsername());
                    if(response.body().getData().getJk().equals("L")){
                        rbMale.setChecked(true);
                        jk = "L";
                    }
                    else{
                        rbFemale.setChecked(true);
                        jk = "P";
                    }
                    setSpinnerKelas(response.body().getData().getId_jurusan());
                    etNim.setText(response.body().getData().getNim());
                    id_dosen = response.body().getData().getId_dosen();
                    id_kelas = response.body().getData().getId_kelas();
                    id_jurusan = response.body().getData().getId_jurusan();
                    nama_kelas = response.body().getData().getKelas().getKelas();
                    nama_dosen = response.body().getData().getDosen().getNama();
                    nama_jurusan = response.body().getData().getJurusan().getJurusan();


                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    progressDialog.hide();
                    Toast.makeText(getApplicationContext(), "Gagal memuat data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                    Log.wtf("sportsid", t.getMessage());
                    finish();
                } else {
                    progressDialog.hide();
                    Log.wtf("sportsid", t.getMessage());
                    Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    void setSpinnerJurusan() {

        mApiService.getListJurusan("7878989sembilan#").enqueue(new Callback<JurusanModel>() {
            @Override
            public void onResponse(Call<JurusanModel> call, Response<JurusanModel> response) {
                jurusan = response.body().getData();
                List<String> namaJurusan = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < jurusan.length; i++) {
                    namaJurusan.add(jurusan[i].getJurusan());
                }
                dataAdapterJurusan = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaJurusan);
                dataAdapterJurusan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spJurusan.setAdapter(dataAdapterJurusan);
                spJurusan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_jurusan = jurusan[i].getId();
                        setSpinnerKelas(id_jurusan);
                        Log.wtf("id_jurusan", id_jurusan);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                spJurusan.setSelection(dataAdapterJurusan.getPosition(nama_jurusan));


            }

            @Override
            public void onFailure(Call<JurusanModel> call, Throwable t) {
                Log.wtf("spinner jurusan", t.getMessage());
            }
        });
    }

    void setSpinnerKelas(String id_jurusan) {
        progressDialog.show();
        mApiService.getListKelas("7878989sembilan#", id_jurusan).enqueue(new Callback<KelasModel>() {
            @Override
            public void onResponse(Call<KelasModel> call, Response<KelasModel> response) {
                kelas = response.body().getData();
                List<String> namaKelas = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < kelas.length; i++) {
                    namaKelas.add(kelas[i].getKelas());
                }
                dataAdapterKelas = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaKelas);
                dataAdapterKelas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spKelas.setAdapter(dataAdapterKelas);
                spKelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_kelas = kelas[i].getId();
                        Log.wtf("id_kelas", id_kelas);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                progressDialog.dismiss();
                spKelas.setSelection(dataAdapterKelas.getPosition(nama_kelas));
            }

            @Override
            public void onFailure(Call<KelasModel> call, Throwable t) {
                Log.wtf("spinner kelas", t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    void setSpinnerDosen() {

        mApiService.getListDosen("7878989sembilan#").enqueue(new Callback<DosenModel>() {
            @Override
            public void onResponse(Call<DosenModel> call, Response<DosenModel> response) {
                dosen = response.body().getData();
                List<String> namaDosen = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < dosen.length; i++) {
                    namaDosen.add(dosen[i].getNama());
                }
                dataAdapterDosen = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaDosen);
                dataAdapterDosen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spDosen.setAdapter(dataAdapterDosen);
                spDosen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_dosen = dosen[i].getId();
                        Log.wtf("id_dosen", id_dosen);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                progressDialog.dismiss();
                spDosen.setSelection(dataAdapterDosen.getPosition(nama_dosen));

            }

            @Override
            public void onFailure(Call<DosenModel> call, Throwable t) {
                Log.wtf("spinner dosen", t.getMessage());
                progressDialog.dismiss();
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                imagePicker.submit(data);
            }
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                cameraImagePicker.submit(data);
            }
        }
    }

}
