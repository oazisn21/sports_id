package com.pt2.sportid.Mahasiswa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.pt2.sportid.Model.ListTugasMahasiswa.Data;
import com.pt2.sportid.Model.ListTugasMahasiswa.ListMahasiswaModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ListMhsAdapter adapter;
    ApiService mApiService;
    TextView datakosong;
    private SharedPreferences sharedPref;
    private ProgressDialog progressDialog;
    Data list;
    public static Activity MahasiswaListAct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.rvlistTugasmhs);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mApiService = ApiUtils.getAPIService(getApplicationContext());

        progressDialog = new ProgressDialog(this);
        MahasiswaListAct = this;
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);
        sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        datakosong = (TextView) findViewById(R.id.tv_datakosongmhs);
        getDataa();

    }
    private void getDataa() {
        progressDialog.show();
        mApiService.getListTugasmhs(sharedPref.getString(getString(R.string.token_pref), "0")).enqueue(new Callback<ListMahasiswaModel>() {
            @Override
            public void onResponse(Call<ListMahasiswaModel> call, Response<ListMahasiswaModel> response) {
                List<String> datalist = new ArrayList<String>();
                if (response.body().getData() != null) {
                    list = response.body().getData();
                    adapter = new ListMhsAdapter(getApplicationContext(), list);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setAdapter(new ListMhsAdapter(getApplicationContext(), list));
                }
                progressDialog.dismiss();

                if(adapter.getItemCount() == 0){
                    datakosong.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ListMahasiswaModel> call, Throwable t) {

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
