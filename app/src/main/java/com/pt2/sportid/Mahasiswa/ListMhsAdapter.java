package com.pt2.sportid.Mahasiswa;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pt2.sportid.DetilTugas;
import com.pt2.sportid.Model.ListTugasMahasiswa.Data;
import com.pt2.sportid.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ListMhsAdapter extends RecyclerView.Adapter<ListMhsAdapter.ViewHolder> {
    private Context context;
    private Data dataList;

    public ListMhsAdapter(Context context, Data dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ListMhsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listtugasmhs,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ListMhsAdapter.ViewHolder holder, int position) {
        holder.tvjudul.setText(dataList.getTugas()[position].getJudul());
        holder.tvkelas.setText(dataList.getTugas()[position].getKelas().getKelas());

        String dateStr = dataList.getTugas()[position].getDeadline();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = sdf.parse(dateStr);
            Date date2 = sdf.parse(sdf.format(new Date()));
            long diff = (date1.getTime() - date2.getTime())/ (1000*60*60*24) ;
//           holder.tvdeadline.setText(diff / (1000*60*60*24)+"");
            holder.tvdeadline.setText(dateStr);
            if(diff < 0){
                holder.tvdeadline.setTextColor(Color.GRAY);
            }
            else{
                holder.tvdeadline.setTextColor(Color.RED);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataList.getTugas().length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvjudul, tvdeadline,tvkelas;

        public ViewHolder(View v) {
            super(v);
            context = v.getContext();

            tvjudul = (TextView) v.findViewById(R.id.judulmhs);
            tvdeadline = (TextView) v.findViewById(R.id.deadlinemhs);
            tvkelas = (TextView) v.findViewById(R.id.tvkelasmhs);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
//                    Intent i = new Intent(context,kumpulkanTugas.class);
//                    i.putExtra("id",dataList.getTugas()[position].getId());
//                    context.startActivity(i);
                    Intent i = new Intent(context, DetilTugas.class);

                    String id_tugas = dataList.getTugas()[position].getId();
                    String judul = dataList.getTugas()[position].getJudul();
                    String deadline = dataList.getTugas()[position].getDeadline();
                    String detil = dataList.getTugas()[position].getKeterangan();
                    String kelas = dataList.getTugas()[position].getKelas().getKelas();
                    try {
                        String id_tugas_selesai = dataList.getTugas_selesai()[position].getId_tugas();
                        i.putExtra("id_tugas_selesai",id_tugas_selesai);
                        Log.d("TAG", "onClick: " + id_tugas_selesai);
                    }
                    catch(ArrayIndexOutOfBoundsException exception) {
                        exception.printStackTrace();
                        String id_tugas_selesai = null;
                    }
//                        !TextUtils.isEmpty(dataList.getTugas_selesai()[position].getId_tugas())

                    i.putExtra("id_tugas", id_tugas);
                    i.putExtra("judul", judul);
                    i.putExtra("deadline", deadline);
                    i.putExtra("detil", detil);
                    i.putExtra("kelas", kelas);
                    i.putExtra("role",2);
                    context.startActivity(i);
                }
            });
        }
    }
}
