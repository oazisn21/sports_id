package com.pt2.sportid.Mahasiswa;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.obsez.android.lib.filechooser.ChooserDialog;
import com.pt2.sportid.DetilTugas;
import com.pt2.sportid.Model.KumpulTugasModel.KumpulTugasModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;
import com.pt2.sportid.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class kumpulkanTugas extends AppCompatActivity {


    String id,namafile = null, extension = null;
    TextView tvFile;
    Button btnPilihFile;
    File file = null;
    EditText etKeterangan;
    private ApiService mApiService;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kumpulkan_tugas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (file != null && file.isFile()){
                    // Get length of file in bytes
                    long fileSizeInBytes = file.length();
                    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    long fileSizeInMB = fileSizeInKB / 1024;
                    if(extension.equals("php")){
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar snackbar = Snackbar.make(parentLayout,"Berkas ilegal!", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                    else if(fileSizeInMB > 5){
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar snackbar = Snackbar.make(parentLayout,"Berkas terlalu besar! (Maks. 5MB)", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                    else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(kumpulkanTugas.this);
                        builder.setCancelable(true);
                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Apakah anda yakin ingin mengumpulkan?");
                        builder.setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        submit();
                                    }
                                });
                        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
                else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(kumpulkanTugas.this);
                    builder.setCancelable(true);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Apakah anda yakin ingin mengumpulkan?");
                    builder.setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    submit();
                                }
                            });
                    builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
        tvFile = (TextView) findViewById(R.id.tv_namaFile);
        btnPilihFile = (Button) findViewById(R.id.btn_pilihFile);
        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        etKeterangan = (EditText) findViewById(R.id.et_keterangan);
        progressDialog.setCancelable(false);
        sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);


        btnPilihFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performFileSearch();
            }
        });

        Bundle b  = getIntent().getExtras();
        id = b.getString("id");

    }

    private void performFileSearch() {
        new ChooserDialog().with(this)
                .withStartFile("/storage/")
                .withChosenListener(new ChooserDialog.Result() {
                    @Override
                    public void onChoosePath(String path, File pathFile) {
                        file = pathFile;
                        namafile = pathFile.getName();
                        extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
                        tvFile.setText(namafile);
                        Log.wtf("l", extension);
                    }
                })
                .build()
                .show();

    }

    RequestBody generate(String text) {
        return RequestBody.create(MediaType.parse("text/plain"), text);
    }
    void submit(){
        //                Intent i = new Intent(kumpulkanTugas.this, MahasiswaList.class);
//                startActivity(i)
        progressDialog.show();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String sekarang = formatter.format(date);
        String keterangan = etKeterangan.getText().toString();
        if(TextUtils.isEmpty(keterangan)){
            keterangan = "-";
        }
        if (file != null && file.isFile()){
            RequestBody reqFile = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part berkas= MultipartBody.Part.createFormData("file", file.getName(), reqFile);
            mApiService.kumpulkan(
                    generate(sharedPref.getString(getString(R.string.token_pref),"0")),
                    generate(id),
                    generate(sekarang),
                    generate(keterangan),
                    berkas).enqueue(new Callback<KumpulTugasModel>() {
                @Override
                public void onResponse(Call<KumpulTugasModel> call, Response<KumpulTugasModel> response) {
                    if(response.isSuccessful()){
                        MahasiswaList.MahasiswaListAct.finish();
                        DetilTugas.DetilTugasAct.finish();
                        Toast.makeText(getApplicationContext(), "Tugas berhasil diunggah!!", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(kumpulkanTugas.this, MahasiswaList.class);
                        startActivity(i);
                        finish();
                    }
                    else{
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal mengunggah data!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<KumpulTugasModel> call, Throwable t) {
                    if (t instanceof NoConnectivityException) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal mengunggah data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                        Log.wtf("sportsid", t.getMessage());
                        finish();
                    } else {
                        progressDialog.dismiss();
                        Log.wtf("sportsid", t.getMessage());
                        Toast.makeText(getApplicationContext(), "Galat tidak diketahui!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
        }
        else{
            mApiService.kumpulkanTanpaFile(
                    generate(sharedPref.getString(getString(R.string.token_pref),"0")),
                    generate(id),
                    generate(sekarang),
                    generate(keterangan)
            ).enqueue(new Callback<KumpulTugasModel>() {
                @Override
                public void onResponse(Call<KumpulTugasModel> call, Response<KumpulTugasModel> response) {
                    if(response.isSuccessful()){
                        MahasiswaList.MahasiswaListAct.finish();
                        DetilTugas.DetilTugasAct.finish();
                        Toast.makeText(getApplicationContext(), "Tugas berhasil diunggah!!", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(kumpulkanTugas.this, MahasiswaList.class);
                        startActivity(i);
                        finish();
                    }
                    else{
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal mengunggah data!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<KumpulTugasModel> call, Throwable t) {
                    if (t instanceof NoConnectivityException) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal mengunggah data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                        Log.wtf("sportsid", t.getMessage());
                        finish();
                    } else {
                        progressDialog.dismiss();
                        Log.wtf("sportsid", t.getMessage());
                        Toast.makeText(getApplicationContext(), "Galat tidak diketahui!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
