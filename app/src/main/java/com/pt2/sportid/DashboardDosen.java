package com.pt2.sportid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pt2.sportid.Dosen.ListTugas;
import com.pt2.sportid.Dosen.ProfilDosen;
import com.pt2.sportid.Model.Logout.LogoutModel;
import com.pt2.sportid.Model.NotificationCounterTugas.NotificationCounterTugasModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardDosen extends AppCompatActivity {

    SharedPreferences sharedPref;
    LinearLayout llLogout, llProfil, llTugas;
    ApiService mApiService;
    ProgressDialog progressDialog;
    TextView tvTugasCounter;
    String nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_dosen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //initComponents
        llLogout = (LinearLayout) findViewById(R.id.ll_logout);
        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);
        View parentLayout = findViewById(android.R.id.content);
        llProfil = (LinearLayout) findViewById(R.id.ll_profil);
        tvTugasCounter = (TextView) findViewById(R.id.badge_notification_1);


        llTugas = (LinearLayout) findViewById(R.id.ll_tugas);
        llTugas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DashboardDosen.this,ListTugas.class);
                startActivity(i);
            }
        });

        //shared preferences
        sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        //setOnClickListener
        llLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        llProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DashboardDosen.this, ProfilDosen.class);
                startActivity(i);
            }
        });

        //get nama
        nama = sharedPref.getString(getString(R.string.nama_pref),"0");

        Snackbar snackbar = Snackbar.make(parentLayout,"Selamat datang " + nama, Snackbar.LENGTH_SHORT);
        snackbar.show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        progressDialog.show();
        String token = sharedPref.getString(getString(R.string.token_pref),"0");
        mApiService.countNewPengumpulan(token).enqueue(new Callback<NotificationCounterTugasModel>() {
            @Override
            public void onResponse(Call<NotificationCounterTugasModel> call, Response<NotificationCounterTugasModel> response) {
                if(response.isSuccessful()){
                    if(response.body().getData().equals("0")){
                        tvTugasCounter.setVisibility(View.GONE);
                        progressDialog.dismiss();
                    }
                    else{
                        tvTugasCounter.setText(response.body().getData());
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationCounterTugasModel> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Mohon periksa internet anda", Toast.LENGTH_SHORT).show();
                    Log.wtf("sportsid", t.getMessage());
                    finish();
                } else {
                    progressDialog.dismiss();
                    Log.wtf("sportsid", t.getMessage());
                    Toast.makeText(getApplicationContext(), "Galat tidak diketahui!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    private void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apakah anda yakin ingin keluar?");
        builder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog.show();
                        String token = sharedPref.getString(getString(R.string.token_pref),"0");
                        mApiService.logout(token).enqueue(new Callback<LogoutModel>() {
                            @Override
                            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                                if(response.body().getStatus().equals("200")){
                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putString(getString(R.string.token_pref), "");
                                    editor.putString(getString(R.string.device_id_pref), "");
                                    editor.putString(getString(R.string.id_pref),"");
                                    editor.putString(getString(R.string.id_role_pref), "");
                                    editor.putString(getString(R.string.username_pref), "");
                                    editor.putString(getString(R.string.nama_pref), "");
                                    editor.putString(getString(R.string.dosen_pref), "");
                                    editor.putString(getString(R.string.jurusan_pref), "");
                                    editor.putString(getString(R.string.email_pref), "");
                                    editor.commit();

                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            }

                            @Override
                            public void onFailure(Call<LogoutModel> call, Throwable t) {
                                if (t instanceof NoConnectivityException) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Proses gagal! Periksa internet anda", Toast.LENGTH_SHORT).show();
                                    Log.wtf("sportsid", t.getMessage());
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    Log.wtf("sportsid", t.getMessage());
                                    Toast.makeText(getApplicationContext(), "Galat tidak diketahui!", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }
                        });
                    }
                });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
