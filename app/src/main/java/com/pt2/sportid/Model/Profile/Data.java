package com.pt2.sportid.Model.Profile;

public class Data
{
    private Jurusan jurusan;

    private String id_jurusan;

    private String jk;

    private String id;

    private String id_user;

    private String id_kelas;

    private String id_dosen;

    private Dosen dosen;

    private Kelas kelas;

    private String kode_dosen;

    private String nama;

    private String foto;

    private String nim;

    private User user;

    public String getKode_dosen() {
        return kode_dosen;
    }

    public void setKode_dosen(String kode_dosen) {
        this.kode_dosen = kode_dosen;
    }

    public Jurusan getJurusan ()
    {
        return jurusan;
    }

    public void setJurusan (Jurusan jurusan)
    {
        this.jurusan = jurusan;
    }

    public String getId_jurusan ()
    {
        return id_jurusan;
    }

    public void setId_jurusan (String id_jurusan)
    {
        this.id_jurusan = id_jurusan;
    }

    public String getJk ()
    {
        return jk;
    }

    public void setJk (String jk)
    {
        this.jk = jk;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getId_user ()
    {
        return id_user;
    }

    public void setId_user (String id_user)
    {
        this.id_user = id_user;
    }

    public String getId_kelas ()
    {
        return id_kelas;
    }

    public void setId_kelas (String id_kelas)
    {
        this.id_kelas = id_kelas;
    }

    public String getId_dosen ()
    {
        return id_dosen;
    }

    public void setId_dosen (String id_dosen)
    {
        this.id_dosen = id_dosen;
    }

    public Dosen getDosen ()
    {
        return dosen;
    }

    public void setDosen (Dosen dosen)
    {
        this.dosen = dosen;
    }

    public Kelas getKelas ()
    {
        return kelas;
    }

    public void setKelas (Kelas kelas)
    {
        this.kelas = kelas;
    }

    public String getNama ()
    {
        return nama;
    }

    public void setNama (String nama)
    {
        this.nama = nama;
    }

    public String getFoto ()
    {
        return foto;
    }

    public void setFoto (String foto)
    {
        this.foto = foto;
    }

    public String getNim ()
    {
        return nim;
    }

    public void setNim (String nim)
    {
        this.nim = nim;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [jurusan = "+jurusan+", id_jurusan = "+id_jurusan+", jk = "+jk+", id = "+id+", id_user = "+id_user+", id_kelas = "+id_kelas+", id_dosen = "+id_dosen+", dosen = "+dosen+", kelas = "+kelas+", nama = "+nama+", foto = "+foto+", nim = "+nim+", user = "+user+"]";
    }
}