package com.pt2.sportid.Model.ListTugasMahasiswa;

public class Tugas {
    private String judul;

    private String id;

    private String id_kelas;

    private String id_dosen;

    private String keterangan;

    private Dosen dosen;

    private Kelas kelas;

    private String deadline;

    public String getJudul ()
    {
        return judul;
    }

    public void setJudul (String judul)
    {
        this.judul = judul;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getId_kelas ()
    {
        return id_kelas;
    }

    public void setId_kelas (String id_kelas)
    {
        this.id_kelas = id_kelas;
    }

    public String getId_dosen ()
    {
        return id_dosen;
    }

    public void setId_dosen (String id_dosen)
    {
        this.id_dosen = id_dosen;
    }

    public String getKeterangan ()
    {
        return keterangan;
    }

    public void setKeterangan (String keterangan)
    {
        this.keterangan = keterangan;
    }

    public Dosen getDosen ()
    {
        return dosen;
    }

    public void setDosen (Dosen dosen)
    {
        this.dosen = dosen;
    }

    public Kelas getKelas ()
    {
        return kelas;
    }

    public void setKelas (Kelas kelas)
    {
        this.kelas = kelas;
    }

    public String getDeadline ()
    {
        return deadline;
    }

    public void setDeadline (String deadline)
    {
        this.deadline = deadline;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [judul = "+judul+", id = "+id+", id_kelas = "+id_kelas+", id_dosen = "+id_dosen+", keterangan = "+keterangan+", dosen = "+dosen+", kelas = "+kelas+", deadline = "+deadline+"]";
    }
}
