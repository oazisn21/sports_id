package com.pt2.sportid.Model.LihatTugas;

public class Data
{
    private String id;

    private String id_tugas;

    private String file;

    private String tanggal_pengumpulan;

    private String file_public_id;

    private String id_mahasiswa;

    private String keterangan;

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    private Mahasiswa mahasiswa;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getId_tugas ()
    {
        return id_tugas;
    }

    public void setId_tugas (String id_tugas)
    {
        this.id_tugas = id_tugas;
    }

    public String getFile ()
    {
        return file;
    }

    public void setFile (String file)
    {
        this.file = file;
    }

    public String getTanggal_pengumpulan ()
    {
        return tanggal_pengumpulan;
    }

    public void setTanggal_pengumpulan (String tanggal_pengumpulan)
    {
        this.tanggal_pengumpulan = tanggal_pengumpulan;
    }

    public String getFile_public_id ()
    {
        return file_public_id;
    }

    public void setFile_public_id (String file_public_id)
    {
        this.file_public_id = file_public_id;
    }

    public String getId_mahasiswa ()
    {
        return id_mahasiswa;
    }

    public void setId_mahasiswa (String id_mahasiswa)
    {
        this.id_mahasiswa = id_mahasiswa;
    }

    public Mahasiswa getMahasiswa ()
    {
        return mahasiswa;
    }

    public void setMahasiswa (Mahasiswa mahasiswa)
    {
        this.mahasiswa = mahasiswa;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", id_tugas = "+id_tugas+", file = "+file+", tanggal_pengumpulan = "+tanggal_pengumpulan+", file_public_id = "+file_public_id+", id_mahasiswa = "+id_mahasiswa+", mahasiswa = "+mahasiswa+"]";
    }
}