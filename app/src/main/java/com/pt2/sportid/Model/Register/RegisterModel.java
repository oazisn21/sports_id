package com.pt2.sportid.Model.Register;

public class RegisterModel
{
    private String status;

    private Sekolah sekolah;

    private Data data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Data getData ()
    {
        return data;
    }

    public Sekolah getSekolah ()

    {
        return sekolah;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}