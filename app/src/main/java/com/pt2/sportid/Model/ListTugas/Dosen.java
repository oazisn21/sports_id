package com.pt2.sportid.Model.ListTugas;

/**
 * Created by Rafishalahudin on 18/04/2018.
 */

public class Dosen {
    private String id;

    private String nama;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getNama ()
    {
        return nama;
    }

    public void setNama (String nama)
    {
        this.nama = nama;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", nama = "+nama+"]";
    }
}
