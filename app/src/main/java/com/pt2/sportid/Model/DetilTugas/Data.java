package com.pt2.sportid.Model.DetilTugas;

public class Data
{
    private Tugas tugas;

    public Tugas getTugas ()
    {
        return tugas;
    }

    public void setTugas (Tugas tugas)
    {
        this.tugas = tugas;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tugas = "+tugas+"]";
    }
}

