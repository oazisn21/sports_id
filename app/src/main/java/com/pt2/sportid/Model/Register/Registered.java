package com.pt2.sportid.Model.Register;
public class Registered
{
    private String id;

    private String id_user;

    private String id_kelas;

    private String id_dosen;

    private String email;

    public String getKode_dosen() {
        return kode_dosen;
    }

    public void setKode_dosen(String kode_dosen) {
        this.kode_dosen = kode_dosen;
    }

    private String kode_dosen;

    private String id_jurusan;

    private String foto;

    private String nama;

    private String nim;

    private String jk;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getId_user ()
    {
        return id_user;
    }

    public void setId_user (String id_user)
    {
        this.id_user = id_user;
    }

    public String getId_kelas ()
    {
        return id_kelas;
    }

    public void setId_kelas (String id_kelas)
    {
        this.id_kelas = id_kelas;
    }

    public String getId_dosen ()
    {
        return id_dosen;
    }

    public void setId_dosen (String id_dosen)
    {
        this.id_dosen = id_dosen;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getId_jurusan ()
    {
        return id_jurusan;
    }

    public void setId_jurusan (String id_jurusan)
    {
        this.id_jurusan = id_jurusan;
    }

    public String getFoto ()
    {
        return foto;
    }

    public void setFoto (String foto)
    {
        this.foto = foto;
    }

    public String getNama ()
    {
        return nama;
    }

    public void setNama (String nama)
    {
        this.nama = nama;
    }

    public String getNim ()
    {
        return nim;
    }

    public void setNim (String nim)
    {
        this.nim = nim;
    }

    public String getJk ()
    {
        return jk;
    }

    public void setJk (String jk)
    {
        this.jk = jk;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", id_user = "+id_user+", id_kelas = "+id_kelas+", id_dosen = "+id_dosen+", email = "+email+", id_jurusan = "+id_jurusan+", foto = "+foto+", nama = "+nama+", nim = "+nim+", jk = "+jk+"]";
    }
}