package com.pt2.sportid.Model.DetilTugas;

public class Kelas
{
    private String id;

    private Jurusan jurusan;

    private String id_jurusan;

    private String kelas;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Jurusan getJurusan ()
    {
        return jurusan;
    }

    public void setJurusan (Jurusan jurusan)
    {
        this.jurusan = jurusan;
    }

    public String getId_jurusan ()
    {
        return id_jurusan;
    }

    public void setId_jurusan (String id_jurusan)
    {
        this.id_jurusan = id_jurusan;
    }

    public String getKelas ()
    {
        return kelas;
    }

    public void setKelas (String kelas)
    {
        this.kelas = kelas;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", jurusan = "+jurusan+", id_jurusan = "+id_jurusan+", kelas = "+kelas+"]";
    }
}
