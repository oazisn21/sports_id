package com.pt2.sportid.Model.CekPengerjaanTugas;

public class Tugas_selesai
{
    private String id_tugas;

    public String getId_tugas ()
    {
        return id_tugas;
    }

    public void setId_tugas (String id_tugas)
    {
        this.id_tugas = id_tugas;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id_tugas = "+id_tugas+"]";
    }
}
