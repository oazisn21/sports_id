package com.pt2.sportid.Model.CekPengerjaanTugas;

public class Data
{
    private Tugas_selesai[] tugas_selesai;

    public Tugas_selesai[] getTugas_selesai ()
    {
        return tugas_selesai;
    }

    public void setTugas_selesai (Tugas_selesai[] tugas_selesai)
    {
        this.tugas_selesai = tugas_selesai;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tugas_selesai = "+tugas_selesai+"]";
    }
}

