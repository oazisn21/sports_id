package com.pt2.sportid.Model.Register;

public class Data
{
    private Token token;

    private Registered registered;

    private User user;

    public Token getToken ()
    {
        return token;
    }

    public void setToken (Token token)
    {
        this.token = token;
    }

    public Registered getRegistered ()
    {
        return registered;
    }

    public void setRegistered (Registered registered)
    {
        this.registered = registered;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [token = "+token+", registered = "+registered+", user = "+user+"]";
    }
}