package com.pt2.sportid.Model.ListTugasMahasiswa;

public class Data {
    private Tugas_selesai[] tugas_selesai;

    private Tugas[] tugas;

    public Tugas_selesai[] getTugas_selesai ()
    {
        return tugas_selesai;
    }

    public void setTugas_selesai (Tugas_selesai[] tugas_selesai)
    {
        this.tugas_selesai = tugas_selesai;
    }

    public Tugas[] getTugas ()
    {
        return tugas;
    }

    public void setTugas (Tugas[] tugas)
    {
        this.tugas = tugas;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tugas_selesai = "+tugas_selesai+", tugas = "+tugas+"]";
    }
}
