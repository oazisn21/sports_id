package com.pt2.sportid.Model.Register;

public class Token
{
    private String id;

    private String id_user;

    private String token;

    private String device_id;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getId_user ()
    {
        return id_user;
    }

    public void setId_user (String id_user)
    {
        this.id_user = id_user;
    }

    public String getToken ()
    {
        return token;
    }

    public void setToken (String token)
    {
        this.token = token;
    }

    public String getDevice_id ()
    {
        return device_id;
    }

    public void setDevice_id (String device_id)
    {
        this.device_id = device_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", id_user = "+id_user+", token = "+token+", device_id = "+device_id+"]";
    }
}