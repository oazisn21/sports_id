package com.pt2.sportid.Model.Profile;

public class Dosen
{
    private String id;

    private String id_user;

    private String kode_dosen;

    private String foto;

    private String nama;

    private String jk;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getId_user ()
    {
        return id_user;
    }

    public void setId_user (String id_user)
    {
        this.id_user = id_user;
    }


    public String getKode_dosen ()
    {
        return kode_dosen;
    }

    public void setKode_dosen (String kode_dosen)
    {
        this.kode_dosen = kode_dosen;
    }

    public String getFoto ()
    {
        return foto;
    }

    public void setFoto (String foto)
    {
        this.foto = foto;
    }

    public String getNama ()
    {
        return nama;
    }

    public void setNama (String nama)
    {
        this.nama = nama;
    }

    public String getJk ()
    {
        return jk;
    }

    public void setJk (String jk)
    {
        this.jk = jk;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", id_user = "+id_user+", kode_dosen = "+kode_dosen+", foto = "+foto+", nama = "+nama+", jk = "+jk+"]";
    }
}