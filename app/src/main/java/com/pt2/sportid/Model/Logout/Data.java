package com.pt2.sportid.Model.Logout;

public class Data
{
    private String logged_out;

    public String getLogged_out ()
    {
        return logged_out;
    }

    public void setLogged_out (String logged_out)
    {
        this.logged_out = logged_out;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [logged_out = "+logged_out+"]";
    }
}