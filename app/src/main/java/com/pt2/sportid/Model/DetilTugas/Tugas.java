package com.pt2.sportid.Model.DetilTugas;

public class Tugas
{
    private String judul;

    private String id;

    private String id_kelas;

    private String id_dosen;

    private String keterangan;

    private String created_at;

    private Kelas kelas;

    private String deadline;

    public String getJudul ()
    {
        return judul;
    }

    public void setJudul (String judul)
    {
        this.judul = judul;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getId_kelas ()
    {
        return id_kelas;
    }

    public void setId_kelas (String id_kelas)
    {
        this.id_kelas = id_kelas;
    }

    public String getId_dosen ()
    {
        return id_dosen;
    }

    public void setId_dosen (String id_dosen)
    {
        this.id_dosen = id_dosen;
    }

    public String getKeterangan ()
    {
        return keterangan;
    }

    public void setKeterangan (String keterangan)
    {
        this.keterangan = keterangan;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public Kelas getKelas ()
    {
        return kelas;
    }

    public void setKelas (Kelas kelas)
    {
        this.kelas = kelas;
    }

    public String getDeadline ()
    {
        return deadline;
    }

    public void setDeadline (String deadline)
    {
        this.deadline = deadline;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [judul = "+judul+", id = "+id+", id_kelas = "+id_kelas+", id_dosen = "+id_dosen+", keterangan = "+keterangan+", created_at = "+created_at+", kelas = "+kelas+", deadline = "+deadline+"]";
    }
}