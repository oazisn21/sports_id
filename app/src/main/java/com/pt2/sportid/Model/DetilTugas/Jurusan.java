package com.pt2.sportid.Model.DetilTugas;

public class Jurusan
{
    private String id;

    private String jurusan;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getJurusan ()
    {
        return jurusan;
    }

    public void setJurusan (String jurusan)
    {
        this.jurusan = jurusan;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", jurusan = "+jurusan+"]";
    }
}
