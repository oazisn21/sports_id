package com.pt2.sportid.Model.Profile;

public class User
{
    private String id;

    private String username;

    private String id_role;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getId_role ()
    {
        return id_role;
    }

    public void setId_role (String id_role)
    {
        this.id_role = id_role;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", username = "+username+", id_role = "+id_role+"]";
    }
}
