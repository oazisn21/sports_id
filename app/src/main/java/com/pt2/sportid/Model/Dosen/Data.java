package com.pt2.sportid.Model.Dosen;

/**
 * Created by Rafishalahudin on 17/04/2018.
 */

public class Data {

    private String id;

    private String nama;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getNama ()
    {
        return nama;
    }

    public void setNama (String nama)
    {
        this.nama = nama;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", nama = "+nama+"]";
    }

}
