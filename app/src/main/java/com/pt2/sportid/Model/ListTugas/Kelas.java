package com.pt2.sportid.Model.ListTugas;

/**
 * Created by Rafishalahudin on 18/04/2018.
 */

public class Kelas {
    private String id;

    private String kelas;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getKelas ()
    {
        return kelas;
    }

    public void setKelas (String kelas)
    {
        this.kelas = kelas;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", kelas = "+kelas+"]";
    }
}
