package com.pt2.sportid.Model.Register;

public class Sekolah
{
    private String id;

    private String sekolah;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSekolah ()
    {
        return sekolah;
    }

    public void setSekolah (String sekolah)
    {
        this.sekolah = sekolah;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", sekolah = "+sekolah+"]";
    }
}