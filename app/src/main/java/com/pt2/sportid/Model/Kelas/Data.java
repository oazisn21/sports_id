package com.pt2.sportid.Model.Kelas;

/**
 * Created by Rafishalahudin on 17/04/2018.
 */

public class Data
{
        private String id;

        private String kelas;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getKelas ()
        {
            return kelas;
        }

        public void setKelas (String kelas)
        {
            this.kelas = kelas;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [id = "+id+", kelas = "+kelas+"]";
        }
    }
