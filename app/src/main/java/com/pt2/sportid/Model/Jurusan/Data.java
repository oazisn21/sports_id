package com.pt2.sportid.Model.Jurusan;

/**
 * Created by Rafishalahudin on 17/04/2018.
 */

public class Data {
    private String id;

    private String jurusan;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getJurusan ()
    {
        return jurusan;
    }

    public void setJurusan (String jurusan)
    {
        this.jurusan = jurusan;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", jurusan = "+jurusan+"]";
    }
}
