package com.pt2.sportid;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnLogin;
    TextView tvDaftar;
    private SharedPreferences sharedPref;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //shared preferences
        sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        //get nama
        String role = sharedPref.getString(getString(R.string.id_role_pref),"0");

        if(role.equals("1")){
            Intent i = new Intent(getApplicationContext(),DashboardDosen.class);
            startActivity(i);
            finish();
        }
        else if (role.equals("2")){
            Intent i = new Intent(getApplicationContext(),DashboardMahasiswa.class);
            startActivity(i);
            finish();
        }
        else{
            //initComponent
            btnLogin = (Button) findViewById(R.id.btn_login_main);
            tvDaftar = (TextView) findViewById(R.id.tv_Daftar);
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Mohon Tunggu...");
            progressDialog.setCancelable(false);
            Dexter.withActivity(this)
                    .withPermissions(Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {

                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                        }
                    }).check();

            //setOnClickListener
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                }
            });
            tvDaftar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, Profil.class);
                    i.putExtra("ini", "Daftar");
                    startActivity(i);
                }
            });
        }




    }
}
