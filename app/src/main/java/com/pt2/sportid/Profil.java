package com.pt2.sportid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.pt2.sportid.Model.Dosen.DosenModel;
import com.pt2.sportid.Model.Jurusan.JurusanModel;
import com.pt2.sportid.Model.Kelas.KelasModel;
import com.pt2.sportid.Model.Register.RegisterModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profil extends AppCompatActivity {

    String ini = null;
    ApiService mApiService;
    Spinner spjurusan, spkelas, spdosen;
    com.pt2.sportid.Model.Dosen.Data[] dosen;
    com.pt2.sportid.Model.Kelas.Data[] kelas;
    com.pt2.sportid.Model.Jurusan.Data[] jurusan;
    EditText etNama, etPassword, etPasswordConfirmation, usmahasiswa, usdosen, etkd_dosen, etnim, etnamamhs, passwordmhs, knfrmpswrd;
    String fotoPath;
    RadioGroup rgGender, rgRole, rgGendermhs;
    RadioButton rbguru, rbsiswa;
    ImageView ivFoto;
    ProgressDialog progressDialog;
    Button btnSubmit;
    String  id_role, gender, gendermhs, id_jurusan, id_kelas, id_dosen;
    boolean llguru = false ;
    boolean llsiswa = false;
    ImagePicker imagePicker;
    CameraImagePicker cameraImagePicker;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //initComponents
        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);

        //perpendekan
        //tahap 1
        final LinearLayout lydosen = (LinearLayout) findViewById(R.id.layout_dosen);
        final LinearLayout lysiswa = (LinearLayout) findViewById(R.id.layout_siswa);
        lydosen.setVisibility(View.INVISIBLE);
        lysiswa.setVisibility(View.INVISIBLE);
        rgRole = (RadioGroup) findViewById(R.id.rg_role);
        ivFoto = (ImageView) findViewById(R.id.iv_upload);
        btnSubmit = (Button) findViewById(R.id.btn_daftar);
        rbguru = (RadioButton) findViewById(R.id.rb_Guru);
        rbsiswa = (RadioButton) findViewById(R.id.rb_Siswa);
        imagePicker = new ImagePicker(Profil.this);
        cameraImagePicker = new CameraImagePicker(Profil.this);

        rbguru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbguru.isChecked() == true) {
                    if(!llguru){
                        initCompguru();
                    }
                    id_role = "1";
                    lydosen.setVisibility(View.VISIBLE);
                    lysiswa.setVisibility(View.GONE);
                    llguru = true;
                }
            }
        });

        rbsiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbsiswa.isChecked() == true) {
                    if(!llsiswa){
                        initCompSiswa();
                    }
                    id_role = "2";
                    lydosen.setVisibility(View.GONE);
                    lysiswa.setVisibility(View.VISIBLE);
                    llsiswa = true;
                }
            }
        });

        //shared preferences
        sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        //setOnClickListener

        ivFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] items = {"Ambil Foto", "Pilih dari penyimpanan",
                        "Batalkan"};
                AlertDialog.Builder builder = new AlertDialog.Builder(Profil.this);
                builder.setTitle("Tambah Foto Diri");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Ambil Foto")) {
                            fotoPath = cameraImagePicker.pickImage();
                        } else if (items[item].equals("Pilih dari penyimpanan")) {
                            imagePicker.pickImage();
                        } else if (items[item].equals("Batalkan")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (id_role.equals("1")) {
                    registerDosen();
                } else if (id_role.equals("2")) {
                    registerMahasiswa();
                }
            }
        });


        //get exra
        Bundle extras = this.getIntent().getExtras();
        if (extras != null) {
            ini = extras.getString("ini");
        }

        setTitle(ini);

        if (ini.equals("Daftar")) {
            imagePicker.setImagePickerCallback(new ImagePickerCallback() {
                @Override
                public void onImagesChosen(List<ChosenImage> list) {
                    Glide.with(getApplicationContext())
                            .load(Uri.fromFile(new File(list.get(0).getOriginalPath())))
                            .into(ivFoto);
                    fotoPath = list.get(0).getOriginalPath();
                    Log.wtf("fotoPath", fotoPath);
                }

                @Override
                public void onError(String s) {
                    Toast.makeText(getApplicationContext(), "error : " + s, Toast.LENGTH_SHORT).show();
                }
            });
            cameraImagePicker.setImagePickerCallback(new ImagePickerCallback() {
                @Override
                public void onImagesChosen(List<ChosenImage> list) {
                    Glide.with(getApplicationContext())
                            .load(fotoPath)
                            .into(ivFoto);
                    Log.wtf("fotoPath", fotoPath);
                }

                @Override
                public void onError(String s) {
                    Toast.makeText(getApplicationContext(), "error : " + s, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    void registerMahasiswa(){
        String nim = etnim.getText().toString();
//        Log.wtf("d",nim);
        String nama = etnamamhs.getText().toString();
//        Log.wtf("d",nama);
        String kelas = spkelas.getSelectedItem().toString();
//        Log.wtf("d",kelas);
        String jurusan = spjurusan.getSelectedItem().toString();
//        Log.wtf("d",jurusan);

        String username = usmahasiswa.getText().toString();
//        Log.wtf("d",username);
        String password = passwordmhs.getText().toString();
//        Log.wtf("d",password);
        String passwordConfirmation = knfrmpswrd.getText().toString();
//        Log.wtf("d",passwordConfirmation);
//        Log.wtf("d",email);
        if (TextUtils.isEmpty(nim) ||
                TextUtils.isEmpty(nama) ||
                TextUtils.isEmpty(password) ||
                TextUtils.isEmpty(kelas) ||
                TextUtils.isEmpty(jurusan) ||
                TextUtils.isEmpty(passwordConfirmation) ||
                TextUtils.isEmpty(username) ||
                TextUtils.isEmpty(id_role) ||
                TextUtils.isEmpty(fotoPath)) {
            Toast.makeText(getApplicationContext(), "Mohon lengkapi kolom yang ada", Toast.LENGTH_SHORT).show();
        } else if (!password.equals(passwordConfirmation)) {
            Toast.makeText(getApplicationContext(), "Konfirmasi password tidak cocok!", Toast.LENGTH_SHORT).show();
        } else {
//                    Daftar
            progressDialog.show();

            File file = new File(fotoPath);

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part image = MultipartBody.Part.createFormData("foto", file.getName(), reqFile);
            mApiService.regis(
                    generate("2"),
                    generate(username),
                    generate(password),
                    generate(nim),
                    generate(nama),
                    generate(id_jurusan),
                    generate(id_dosen),
                    generate(id_kelas),
                    generate(gendermhs),
                    image,
                    generate("999") //device id
            ).enqueue(new Callback<RegisterModel>() {
                @Override
                public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equals("200")) {
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.nim), response.body().getData().getRegistered().getNim());
                            editor.putString(getString(R.string.role_pref), response.body().getData().getUser().getId_role());
                            editor.putString(getString(R.string.username_pref), response.body().getData().getUser().getUsername());
                            editor.putString(getString(R.string.id_dosen_pref), response.body().getData().getRegistered().getId_dosen());
                            editor.putString(getString(R.string.gender), response.body().getData().getRegistered().getJk());
                            editor.putString(getString(R.string.nama_pref), response.body().getData().getRegistered().getNama());
                            editor.putString(getString(R.string.email_pref), response.body().getData().getRegistered().getEmail());
                            editor.putString(getString(R.string.token_pref), response.body().getData().getToken().getToken());
                            editor.commit();
                            Intent i = new Intent(getApplicationContext(), DashboardMahasiswa.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); //kill old activity
                            startActivity(i);
                            finish();
                        }
                    } else {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), "Username atau NIM telah terdaftar!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RegisterModel> call, Throwable t) {
                    if (t instanceof NoConnectivityException) {
                        progressDialog.hide();
                        Snackbar snackbar = Snackbar.make(getCurrentFocus(), "Gagal mendaftar! Cek internet anda.", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        Log.wtf("sportsid", t.getMessage());
                    } else {
                        progressDialog.hide();
                        Log.wtf("sportsid", t.getMessage());
                        Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    void registerDosen(){
        String kd_dosen = etkd_dosen.getText().toString();
        String nama = etNama.getText().toString();
        String username = usdosen.getText().toString();
        String password = etPassword.getText().toString();
        String passwordConfirmation = etPasswordConfirmation.getText().toString();
        if (TextUtils.isEmpty(kd_dosen) ||
                TextUtils.isEmpty(nama) ||
                TextUtils.isEmpty(password) ||
                TextUtils.isEmpty(passwordConfirmation) ||
                TextUtils.isEmpty(username) ||
                TextUtils.isEmpty(id_role) ||
                TextUtils.isEmpty(fotoPath)) {
            Log.wtf("meh","meeeh");
            Toast.makeText(getApplicationContext(), "Mohon lengkapi kolom yang ada", Toast.LENGTH_SHORT).show();
        } else if (!password.equals(passwordConfirmation)) {
            Toast.makeText(getApplicationContext(), "Konfirmasi password tidak cocok!", Toast.LENGTH_SHORT).show();
        } else {
//                    Daftar
            progressDialog.show();

            File file = new File(fotoPath);

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part image = MultipartBody.Part.createFormData("foto", file.getName(), reqFile);
            mApiService.regisdosen(
                    generate("1"),
                    generate(username),
                    generate(password),
                    generate(nama),
                    generate(gender),
                    image,
                    generate("999"), //device id
                    generate(kd_dosen)
            ).enqueue(new Callback<RegisterModel>() {
                @Override
                public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equals("200")) {
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.id_dosen_pref), response.body().getData().getRegistered().getId());
                            editor.putString(getString(R.string.role_pref), response.body().getData().getUser().getId_role());
                            editor.putString(getString(R.string.username_pref), response.body().getData().getUser().getUsername());
                            editor.putString(getString(R.string.kd_dosen_pref), response.body().getData().getRegistered().getKode_dosen());
                            editor.putString(getString(R.string.gender), response.body().getData().getRegistered().getJk());
                            editor.putString(getString(R.string.nama_pref), response.body().getData().getRegistered().getNama());
                            editor.putString(getString(R.string.email_pref), response.body().getData().getRegistered().getEmail());
                            editor.putString(getString(R.string.token_pref), response.body().getData().getToken().getToken());
                            editor.commit();
                            Intent i = new Intent(getApplicationContext(), DashboardDosen.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); //kill old activity
                            startActivity(i);
                            finish();
                        }
                    } else {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), "Username atau Kode Dosen telah terdaftar!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RegisterModel> call, Throwable t) {
                    if (t instanceof NoConnectivityException) {
                        progressDialog.hide();
                        Snackbar snackbar = Snackbar.make(getCurrentFocus(), "Gagal mendaftar! Cek internet anda.", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        Log.wtf("sportsid", t.getMessage());
                    } else {
                        progressDialog.hide();
                        Log.wtf("sportsid", t.getMessage());
                        Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    private void initCompSiswa() {
        progressDialog.show();
        etnim = (EditText) findViewById(R.id.et_nim);
        etnamamhs = (EditText) findViewById(R.id.et_nama);
        spkelas = (Spinner) findViewById(R.id.s_kelas);
        spjurusan = (Spinner) findViewById(R.id.s_jurusan);
        spdosen = (Spinner) findViewById(R.id.s_dosen);
        usmahasiswa = (EditText) findViewById(R.id.et_usernamemahasiswa);
        passwordmhs = (EditText) findViewById(R.id.et_password);
        knfrmpswrd = (EditText) findViewById(R.id.et_password_confirm);
        rgGendermhs = (RadioGroup) findViewById(R.id.rg_gendermhs);
        rgGendermhs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rb_l) {
                    gendermhs = "L";
                } else if (i == R.id.rb_p) {
                    gendermhs = "P";
                }
            }
        });
        setSpinnerJurusan();
        setSpinnerDosen();
    }

    private void initCompguru() {
        progressDialog.show();
        etkd_dosen = (EditText) findViewById(R.id.et_kddosen);
        etNama = (EditText) findViewById(R.id.et_namadosen);
        etPassword = (EditText) findViewById(R.id.et_passworddosen);
        etPasswordConfirmation = (EditText) findViewById(R.id.et_password_confirmdosen);
        usdosen = (EditText) findViewById(R.id.et_username);
        rgGender = (RadioGroup) findViewById(R.id.rg_genderdosen);

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rb_ldosen) {
                    gender = "L";
                } else if (i == R.id.rb_pdosen) {
                    gender = "P";
                }
            }
        });

        progressDialog.dismiss();
    }

    RequestBody generate(String text) {
        return RequestBody.create(MediaType.parse("text/plain"), text);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                imagePicker.submit(data);
            }
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                cameraImagePicker.submit(data);
            }
        }
    }
    void setSpinnerJurusan() {

        mApiService.getListJurusan("7878989sembilan#").enqueue(new Callback<JurusanModel>() {
            @Override
            public void onResponse(Call<JurusanModel> call, Response<JurusanModel> response) {
                jurusan = response.body().getData();
                List<String> namaJurusan = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < jurusan.length; i++) {
                    namaJurusan.add(jurusan[i].getJurusan());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaJurusan);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spjurusan.setAdapter(dataAdapter);
                spjurusan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_jurusan = jurusan[i].getId();
                        setSpinnerKelas(id_jurusan);
                        Log.wtf("id_jurusan", id_jurusan);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }

            @Override
            public void onFailure(Call<JurusanModel> call, Throwable t) {
                Log.wtf("spinner jurusan", t.getMessage());
            }
        });
    }

    void setSpinnerKelas(String id_jurusan) {
        progressDialog.show();
        mApiService.getListKelas("7878989sembilan#", id_jurusan).enqueue(new Callback<KelasModel>() {
            @Override
            public void onResponse(Call<KelasModel> call, Response<KelasModel> response) {
                kelas = response.body().getData();
                List<String> namaKelas = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < kelas.length; i++) {
                    namaKelas.add(kelas[i].getKelas());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaKelas);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spkelas.setAdapter(dataAdapter);
                spkelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_kelas = kelas[i].getId();
                        Log.wtf("id_kelas", id_kelas);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<KelasModel> call, Throwable t) {
                Log.wtf("spinner kelas", t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    void setSpinnerDosen() {

        mApiService.getListDosen("7878989sembilan#").enqueue(new Callback<DosenModel>() {
            @Override
            public void onResponse(Call<DosenModel> call, Response<DosenModel> response) {
                dosen = response.body().getData();
                List<String> namaDosen = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < dosen.length; i++) {
                    namaDosen.add(dosen[i].getNama());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaDosen);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spdosen.setAdapter(dataAdapter);
                spdosen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_dosen = dosen[i].getId();
                        Log.wtf("id_dosen", id_dosen);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<DosenModel> call, Throwable t) {
                Log.wtf("spinner dosen", t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    //back button onclick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
