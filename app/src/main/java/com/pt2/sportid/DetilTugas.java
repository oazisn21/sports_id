package com.pt2.sportid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pt2.sportid.Mahasiswa.kumpulkanTugas;
import com.pt2.sportid.Model.CekPengerjaanTugas.CekPengerjaanTugasModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetilTugas extends AppCompatActivity {

    EditText etjudul, etdetil, etdeadline, etkelas;
    Button btnSubmit;
    String id_tugas, deadline;
    boolean dikerjakan;
    int role;
    public static Activity DetilTugasAct;
    private ApiService mApiService;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_tugas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        etjudul = (EditText) findViewById(R.id.et_judul);
        etdetil = (EditText) findViewById(R.id.et_keterangan);
        etdeadline = (EditText) findViewById(R.id.et_deadline);
        etkelas = (EditText) findViewById(R.id.et_kelas);
        btnSubmit = (Button) findViewById(R.id.btn_kumpul);
        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);
        DetilTugasAct = this;
        //shared preferences
        sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        etjudul.setFocusable(false);
        etdetil.setFocusable(false);
        etdeadline.setFocusable(false);
        etkelas.setFocusable(false);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        id_tugas = b.getString("id_tugas");
        etjudul.setText(b.getString("judul"));
        etdetil.setText(b.getString("detil"));
        etdeadline.setText(b.getString("deadline"));
        etkelas.setText(b.getString("kelas"));
        role = b.getInt("role");
        deadline = b.getString("deadline");

        if (role != 1){
            progressDialog.show();
            long diff = 0;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = sdf.parse(deadline);
                Date date2 = sdf.parse(sdf.format(new Date()));
                diff = (date1.getTime() - date2.getTime())/ (1000*60*60*24) ;
                Log.d("TAG", "diff: " + diff);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            mApiService.cekPengerjaanTugas(
                    sharedPref.getString(getString(R.string.token_pref),"0"),
                    id_tugas).enqueue(new Callback<CekPengerjaanTugasModel>() {
                @Override
                public void onResponse(Call<CekPengerjaanTugasModel> call, Response<CekPengerjaanTugasModel> response) {
                    if(response.body().getData().getTugas_selesai() != null){
                        int i = 0;
                        for (i = 0; i < response.body().getData().getTugas_selesai().length; i++){
                            if(response.body().getData().getTugas_selesai()[i].getId_tugas().equals(id_tugas)){
                                dikerjakan = true;
                                btnSubmit.setVisibility(View.GONE);
                            }
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<CekPengerjaanTugasModel> call, Throwable t) {
                    if (t instanceof NoConnectivityException) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal memuat data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                        Log.wtf("sportsid", t.getMessage());
                        finish();
                    } else {
                        progressDialog.dismiss();
                        Log.wtf("sportsid", t.getMessage());
                        Toast.makeText(getApplicationContext(), "Galat tidak diketahui!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });

            if(diff < 0){
                btnSubmit.setVisibility(View.GONE);
            }
            else{
                btnSubmit.setVisibility(View.VISIBLE);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(DetilTugas.this,kumpulkanTugas.class);
                        i.putExtra("id", id_tugas);
                        startActivity(i);
                    }
                });
            }

        }
        else{
            btnSubmit.setVisibility(View.GONE);
        }


    }

    //back button onclick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
