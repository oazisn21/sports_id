package com.pt2.sportid.Dosen.LihatTugasView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pt2.sportid.Model.LihatTugas.Data;
import com.pt2.sportid.Model.LihatTugas.LihatTugasModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class lihatTugasView extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextView tvDataKosong;
    private tugasLihatAdapter adapter;
    ApiService mApiService;
    String id_tugas;
    private SharedPreferences sharedPref;
    private ProgressDialog progressDialog;
    Data[] list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_tugas_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvDataKosong = findViewById(R.id.tv_datakosong);

        recyclerView = (RecyclerView) findViewById(R.id.listlihattugas);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mApiService = ApiUtils.getAPIService(getApplicationContext());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);
        sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        getData();
    }

    private void getData() {
        Intent i = getIntent();
        Bundle b = i.getExtras();
        id_tugas = b.getString("id_tugas");

        progressDialog.show();
        mApiService.lihatTugas(
                sharedPref.getString(getString(R.string.token_pref), "0"),
                id_tugas).enqueue(new Callback<LihatTugasModel>() {
            @Override
            public void onResponse(Call<LihatTugasModel> call, Response<LihatTugasModel> response) {
                List<String> datalist = new ArrayList<String>();
                if (response.code() == 200) {
                    if (response.body() != null) {

                        list = response.body().getData();
                        adapter = new tugasLihatAdapter(getApplicationContext(), list);

                        recyclerView.setAdapter(adapter);
                        recyclerView.setAdapter(new tugasLihatAdapter(getApplicationContext(), list));
                    }

                }
                Toast.makeText(getBaseContext(),"Klik nama untuk mengunduh hasil pengerjaan", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

                if(adapter.getItemCount() == 0){
                    tvDataKosong.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<LihatTugasModel> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
