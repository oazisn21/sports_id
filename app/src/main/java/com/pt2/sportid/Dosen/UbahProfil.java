package com.pt2.sportid.Dosen;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.pt2.sportid.Model.EditProfil.EditProfilModel;
import com.pt2.sportid.Model.Profile.ProfileModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;
import com.pt2.sportid.R;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahProfil extends AppCompatActivity {

    private ApiService mApiService;
    ImageView ivFotoProfil;
    EditText etKodeDosen, etNama, etUsername, etPasswordOld, etPasswordNew, etConfirmPasswordNew;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPref;;
    RadioButton rbMale, rbFemale;
    Button btnSimpan;
    String url,fotoPath, jk;
    private ImagePicker imagePicker;
    private CameraImagePicker cameraImagePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_profil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //initComponents
        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);
        etKodeDosen = (EditText) findViewById(R.id.et_kddosen);
        etNama = (EditText) findViewById(R.id.et_namadosen);
        etUsername = (EditText) findViewById(R.id.et_username);
        rbMale = (RadioButton) findViewById(R.id.rb_ldosen);
        rbFemale = (RadioButton) findViewById(R.id.rb_pdosen);
        etPasswordOld = (EditText) findViewById(R.id.et_passwordlamadosen);
        etPasswordNew = (EditText) findViewById(R.id.et_passworddosen);
        etConfirmPasswordNew = (EditText) findViewById(R.id.et_password_confirmdosen);
        ivFotoProfil = (ImageView) findViewById(R.id.iv_upload);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        imagePicker = new ImagePicker(this);
        cameraImagePicker = new CameraImagePicker(this);

        //shared preferences
        sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        progressDialog.show();
        mApiService.getMyProfile(sharedPref.getString(getString(R.string.token_pref),"0")).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                if(response.isSuccessful()){
                    Glide.with(getApplicationContext())
                            .load(response.body().getData().getFoto())
                            .into(ivFotoProfil);
                    url = response.body().getData().getFoto();
                    etKodeDosen.setText(response.body().getData().getKode_dosen());
                    etNama.setText(response.body().getData().getNama());
                    etUsername.setText(response.body().getData().getUser().getUsername());
                    if(response.body().getData().getJk().equals("L")){
                        rbMale.setChecked(true);
                        jk = "L";
                    }
                    else{
                        rbFemale.setChecked(true);
                        jk = "P";

                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    progressDialog.hide();
                    Toast.makeText(getApplicationContext(), "Gagal memuat data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                    Log.wtf("sportsid", t.getMessage());
                    finish();
                } else {
                    progressDialog.hide();
                    Log.wtf("sportsid", t.getMessage());
                    Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerDosen();
            }
        });

        imagePicker.setImagePickerCallback(new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> list) {
                Glide.with(getApplicationContext())
                        .load(Uri.fromFile(new File(list.get(0).getOriginalPath())))
                        .into(ivFotoProfil);
                fotoPath = list.get(0).getOriginalPath();
                Log.wtf("fotoPath", fotoPath);
            }

            @Override
            public void onError(String s) {
                Toast.makeText(getApplicationContext(), "error : " + s, Toast.LENGTH_SHORT).show();
            }
        });
        cameraImagePicker.setImagePickerCallback(new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> list) {
                Glide.with(getApplicationContext())
                        .load(fotoPath)
                        .into(ivFotoProfil);
                Log.wtf("fotoPath", fotoPath);
            }

            @Override
            public void onError(String s) {
                Toast.makeText(getApplicationContext(), "error : " + s, Toast.LENGTH_SHORT).show();
            }
        });

        ivFotoProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] items = {"Ambil Foto", "Pilih dari penyimpanan",
                        "Batalkan"};
                AlertDialog.Builder builder = new AlertDialog.Builder(UbahProfil.this);
                builder.setTitle("Tambah Foto Diri");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Ambil Foto")) {
                            fotoPath = cameraImagePicker.pickImage();
                        } else if (items[item].equals("Pilih dari penyimpanan")) {
                            imagePicker.pickImage();
                        } else if (items[item].equals("Batalkan")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
        rbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jk = "L";
            }
        });
        rbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jk = "P";
            }
        });


    }

    RequestBody generate(String text) {
        return RequestBody.create(MediaType.parse("text/plain"), text);
    }


    void registerDosen(){
        //etKodeDosen, etNama, etEmail, etUsername, etPasswordOld, etPasswordNew, etConfirmPasswordNew;
        final String kd_dosen = etKodeDosen.getText().toString();
        final String nama = etNama.getText().toString();
        final String username = etUsername.getText().toString();
        String password = etPasswordOld.getText().toString();
        final String passwordNew = etPasswordNew.getText().toString();
        String passwordConfirmationNew = etConfirmPasswordNew.getText().toString();
        if (TextUtils.isEmpty(kd_dosen) ||
                TextUtils.isEmpty(nama) ||
                TextUtils.isEmpty(username) ||
                TextUtils.isEmpty(jk)){
            Log.wtf("meh","meeeh");
            Toast.makeText(getApplicationContext(), "Mohon lengkapi kolom yang ada", Toast.LENGTH_SHORT).show();
        } else if (!TextUtils.isEmpty(password) ) {
            if (TextUtils.isEmpty(passwordNew) || TextUtils.isEmpty(passwordConfirmationNew)) {
                Toast.makeText(getApplicationContext(), "Mohon lengkapi kolom yang ada!", Toast.LENGTH_SHORT).show();
            }
        }
        else if(!passwordNew.equals(passwordConfirmationNew)){
            Toast.makeText(getApplicationContext(), "Konfirmasi kata sandi tidak cocok!", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(fotoPath)){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Konfirmasi");
            builder.setMessage("Apakah anda yakin dengan data ini?");
            builder.setPositiveButton("Ya",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressDialog.show();

                            mApiService.editProfilDosen(
                                    generate(sharedPref.getString(getString(R.string.token_pref),"0")),
                                    generate("1"),
                                    generate(username),
                                    generate(passwordNew),
                                    generate(nama),
                                    generate(jk),
                                    null,
                                    generate(kd_dosen)
                            ).enqueue(new Callback<EditProfilModel>() {
                                @Override
                                public void onResponse(Call<EditProfilModel> call, Response<EditProfilModel> response) {
                                    if(response.isSuccessful()){
                                        Toast.makeText(getApplicationContext(),"Data berasil berubah",Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                    else{
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(),"Data gagal berubah",Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<EditProfilModel> call, Throwable t) {
                                    if (t instanceof NoConnectivityException) {
                                        progressDialog.hide();
                                        Toast.makeText(getApplicationContext(), "Gagal merubah data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                                        Log.wtf("sportsid", t.getMessage());
                                        finish();
                                    } else {
                                        progressDialog.hide();
                                        Log.wtf("sportsid", t.getMessage());
                                        Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            });

                        }
                    });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();


        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Konfirmasi");
            builder.setMessage("Apakah anda yakin dengan data ini?");
            builder.setPositiveButton("Ya",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressDialog.show();

                            File file = new File(fotoPath);

                            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                            MultipartBody.Part image = MultipartBody.Part.createFormData("foto", file.getName(), reqFile);

                            mApiService.editProfilDosen(
                                    generate(sharedPref.getString(getString(R.string.token_pref),"0")),
                                    generate("1"),
                                    generate(username),
                                    generate(passwordNew),
                                    generate(nama),
                                    generate(jk),
                                    image,
                                    generate(kd_dosen)
                            ).enqueue(new Callback<EditProfilModel>() {
                                @Override
                                public void onResponse(Call<EditProfilModel> call, Response<EditProfilModel> response) {
                                    if(response.isSuccessful() && response.body().getStatus().equals(200)){
                                        Toast.makeText(getApplicationContext(),"Data berasil berubah",Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                    else{
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(),"Data gagal berubah",Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<EditProfilModel> call, Throwable t) {
                                    if (t instanceof NoConnectivityException) {
                                        progressDialog.hide();
                                        Toast.makeText(getApplicationContext(), "Gagal merubah data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                                        Log.wtf("sportsid", t.getMessage());
                                        finish();
                                    } else {
                                        progressDialog.hide();
                                        Log.wtf("sportsid", t.getMessage());
                                        Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            });

                        }
                    });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                imagePicker.submit(data);
            }
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                cameraImagePicker.submit(data);
            }
        }
    }

    //back button onclick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
