package com.pt2.sportid.Dosen;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.pt2.sportid.DetilTugas;
import com.pt2.sportid.Dosen.LihatTugasView.lihatTugasView;
import com.pt2.sportid.Model.ListTugas.Data;
import com.pt2.sportid.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rafishalahudin on 18/04/2018.
 */

public class ListTugasAdapter extends RecyclerView.Adapter<ListTugasAdapter.ViewHolder> {

    private Context context;
    private Data[] dataList;
    int position;

    public ListTugasAdapter(Context context, Data[] dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ListTugasAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listtugasrv,parent,false);
        return new ViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull final ListTugasAdapter.ViewHolder holder, int position) {
        holder.tvjudul.setText(dataList[position].getJudul());
        holder.tvKelass.setText(dataList[position].getKelas().getKelas());

            holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(context ,holder.buttonViewOption);
                //inflating menu from xml resource
                popup.inflate(R.menu.option_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.lihat:
                                int positionn = holder.getAdapterPosition();
                                String id_tugass = dataList[positionn].getId();
                                Intent i = new Intent(context, lihatTugasView.class);
                                i.putExtra("id_tugas", id_tugass);
                                context.startActivity(i);

                                break;

                            case R.id.edit:
                                int position = holder.getAdapterPosition();
                                String id_tugas = dataList[position].getId();
                                String judul = dataList[position].getJudul();
                                String deadline = dataList[position].getDeadline();
                                String detil = dataList[position].getKeterangan();
                                String kelas = dataList[position].getKelas().getKelas();

                                Intent ii = new Intent(context,EditTugas.class);
                                ii.putExtra("id_tugas", id_tugas);
                                ii.putExtra("judul", judul);
                                ii.putExtra("deadline", deadline);
                                ii.putExtra("detil", detil);
                                ii.putExtra("kelas", kelas);
                                ii.putExtra("role",1);
                                context.startActivity(ii);
//                                Toast.makeText(context,"Fitur belum diimplementasikan",Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setCancelable(true);
                                builder.setTitle("Konfirmasi");
                                builder.setMessage("Apakah anda yakin ingin menghapusnya?");
                                builder.setPositiveButton("Ya",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                int position = holder.getAdapterPosition();
                                                String id_tugas = dataList[position].getId();

                                                //shared preferences
                                                SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                                                ListTugas.hapus(id_tugas,sharedPref.getString(context.getString(R.string.token_pref),"0"));
                                            }
                                        });
                                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog dialog = builder.create();
                                dialog.show();
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

            }
        });
        String dateStr = dataList[position].getDeadline();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
           Date date1 = sdf.parse(dateStr);
           Date date2 = sdf.parse("2018-03-22");
           long diff = date1.getTime() - date2.getTime();
           holder.tvdeadline.setText(dateStr);
           if(diff < 0){
               holder.tvdeadline.setTextColor(Color.GRAY);
           }
        } catch (ParseException e) {
            e.printStackTrace();
        }



    }

    @Override
    public int getItemCount() {
        return dataList.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvjudul,tvdeadline,tvKelass;
        public ImageButton buttonViewOption;
        public ViewHolder(View v){
            super(v);
            context = v.getContext();
            tvjudul = (TextView) v.findViewById(R.id.tvjudul);
            tvdeadline = (TextView) v.findViewById(R.id.tvdeadline);
            tvKelass = (TextView) v.findViewById(R.id.tvkelas);
            buttonViewOption = (ImageButton) v.findViewById(R.id.textViewOptions);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    String id_tugas = dataList[position].getId();
                    String judul = dataList[position].getJudul();
                    String deadline = dataList[position].getDeadline();
                    String detil = dataList[position].getKeterangan();
                    String kelas = dataList[position].getKelas().getKelas();


                    Intent i = new Intent(context, DetilTugas.class);
                    i.putExtra("id_tugas", id_tugas);
                    i.putExtra("judul", judul);
                    i.putExtra("deadline", deadline);
                    i.putExtra("detil", detil);
                    i.putExtra("kelas", kelas);
                    i.putExtra("role",1);
                    context.startActivity(i);
                }
            });

        }
    }

}
