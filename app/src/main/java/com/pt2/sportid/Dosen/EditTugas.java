package com.pt2.sportid.Dosen;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.pt2.sportid.MainActivity;
import com.pt2.sportid.Model.DetilTugas.DetilTugasModel;
import com.pt2.sportid.Model.EditProfil.EditProfilModel;
import com.pt2.sportid.Model.Jurusan.JurusanModel;
import com.pt2.sportid.Model.Kelas.KelasModel;
import com.pt2.sportid.Model.Logout.LogoutModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;
import com.pt2.sportid.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditTugas extends AppCompatActivity {

    ApiService mApiService;
    com.pt2.sportid.Model.Kelas.Data[] kelas;
    com.pt2.sportid.Model.Jurusan.Data[] jurusan;
    EditText etjudul,etKeterangan,etDeadline;
    Button btnSimpan;
    Calendar myCalendar;
    ArrayAdapter<String> dataAdapterJurusan, dataAdapterKelas;
    String nama_jurusan;
    Spinner spKelas,spJurusan;
    String id_kelas,id_jurusan;
    ProgressDialog progressDialog;
    private SharedPreferences sharedPref;
    private String nama_kelas;
    private String id_tugas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_tugas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        spKelas = (Spinner) findViewById(R.id.spkelas);
        btnSimpan= (Button) findViewById(R.id.btn_submit);
        spJurusan= (Spinner) findViewById(R.id.spjurusan);
        etjudul = (EditText) findViewById(R.id.et_judul);
        etKeterangan = (EditText) findViewById(R.id.et_keterangan);
        etDeadline = (EditText) findViewById(R.id.et_deadline);

        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);

        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        etDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditTugas.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String judul = etjudul.getText().toString();
                final String keterangan = etKeterangan.getText().toString();
                final String deadline = etDeadline.getText().toString();
                if (TextUtils.isEmpty(judul) ||
                        TextUtils.isEmpty(keterangan) ||
                        TextUtils.isEmpty(deadline) ||
                        TextUtils.isEmpty(id_kelas) ||
                        TextUtils.isEmpty(id_jurusan)){
                    Toast.makeText(getApplicationContext(), "Mohon lengkapi kolom yang ada", Toast.LENGTH_SHORT).show();
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditTugas.this);
                    builder.setCancelable(true);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Apakah anda yakin dengan data ini?");
                    builder.setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressDialog.show();
                                    String id_dosen = sharedPref.getString(getString(R.string.id_dosen_pref),"0");
                                    String token = sharedPref.getString(getString(R.string.token_pref),"0");
                                    mApiService.editTugas(
                                            token,
                                            judul,
                                            keterangan,
                                            id_dosen,
                                            id_kelas,
                                            deadline,
                                            id_tugas
                                    ).enqueue(new Callback<EditProfilModel>() {
                                        @Override
                                        public void onResponse(Call<EditProfilModel> call, Response<EditProfilModel> response) {
                                            if(response.isSuccessful()){
                                                if(response.body().getStatus().equals("200")){
                                                    Toast.makeText(getApplicationContext(),"Data berasil berubah",Toast.LENGTH_SHORT).show();
                                                    finish();
                                                }
                                            }
                                            else{
                                                Toast.makeText(getApplicationContext(),"Data gagal dirubah",Toast.LENGTH_SHORT).show();
                                                progressDialog.dismiss();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<EditProfilModel> call, Throwable t) {
                                            if (t instanceof NoConnectivityException) {
                                                progressDialog.hide();
                                                Toast.makeText(getApplicationContext(), "Gagal merubah data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                                                Log.wtf("sportsid", t.getMessage());
                                                finish();
                                            } else {
                                                progressDialog.hide();
                                                Log.wtf("sportsid", t.getMessage());
                                                Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        }
                                    });
                                }
                            });
                    builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();


                }
            }
        });

        progressDialog.show();
        showData();

    }

    void showData(){
        Intent i = getIntent();
        Bundle b = i.getExtras();
        id_tugas = b.getString("id_tugas");
        etjudul.setText(b.getString("judul"));
        etKeterangan.setText(b.getString("detil"));
        etDeadline.setText(b.getString("deadline"));

        progressDialog.show();
        String token = sharedPref.getString(getString(R.string.token_pref),"0");
        mApiService.getDetilTugas(token,id_tugas).enqueue(new Callback<DetilTugasModel>() {
            @Override
            public void onResponse(Call<DetilTugasModel> call, Response<DetilTugasModel> response) {
                if(response.code() == 200){
                    if(response.body().getData()!=null){
                        nama_jurusan = response.body().getData().getTugas().getKelas().getJurusan().getJurusan();
                        nama_kelas = response.body().getData().getTugas().getKelas().getKelas();
                        id_kelas = response.body().getData().getTugas().getKelas().getId();
                        id_jurusan = response.body().getData().getTugas().getKelas().getId_jurusan();
                        setSpinnerJurusan();
                    }
                }
            }

            @Override
            public void onFailure(Call<DetilTugasModel> call, Throwable t) {

            }
        });

    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etDeadline.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void setSpinnerJurusan() {

        mApiService.getListJurusan("7878989sembilan#").enqueue(new Callback<JurusanModel>() {
            @Override
            public void onResponse(Call<JurusanModel> call, Response<JurusanModel> response) {
                jurusan = response.body().getData();
                List<String> namaJurusan = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < jurusan.length; i++) {
                    namaJurusan.add(jurusan[i].getJurusan());
                }
                dataAdapterJurusan = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaJurusan);
                dataAdapterJurusan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spJurusan.setAdapter(dataAdapterJurusan);
                spJurusan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_jurusan = jurusan[i].getId();
                        setSpinnerKelas(id_jurusan);
                        Log.wtf("id_jurusan", id_jurusan);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                spJurusan.setSelection(dataAdapterJurusan.getPosition(nama_jurusan));


            }

            @Override
            public void onFailure(Call<JurusanModel> call, Throwable t) {
                Log.wtf("spinner jurusan", t.getMessage());
            }
        });
    }

    void setSpinnerKelas(String id_jurusan) {
        progressDialog.show();
        mApiService.getListKelas("7878989sembilan#", id_jurusan).enqueue(new Callback<KelasModel>() {
            @Override
            public void onResponse(Call<KelasModel> call, Response<KelasModel> response) {
                kelas = response.body().getData();
                List<String> namaKelas = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < kelas.length; i++) {
                    namaKelas.add(kelas[i].getKelas());
                }
                dataAdapterKelas = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaKelas);
                dataAdapterKelas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spKelas.setAdapter(dataAdapterKelas);
                spKelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_kelas = kelas[i].getId();
                        Log.wtf("id_kelas", id_kelas);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                progressDialog.dismiss();
                spKelas.setSelection(dataAdapterKelas.getPosition(nama_kelas));
            }

            @Override
            public void onFailure(Call<KelasModel> call, Throwable t) {
                Log.wtf("spinner kelas", t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

}
