package com.pt2.sportid.Dosen.LihatTugasView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pt2.sportid.Model.LihatTugas.Data;
import com.pt2.sportid.R;

public class tugasLihatAdapter extends RecyclerView.Adapter<tugasLihatAdapter.ViewHolder>  {

    private Context context;
    private Data[] listData;

    public tugasLihatAdapter(Context context, Data[] listData) {
        this.context = context;
        this.listData = listData;
    }

    @NonNull
    @Override
    public tugasLihatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_lihat_tugas,parent,false);
        return new tugasLihatAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull tugasLihatAdapter.ViewHolder holder, int position) {
        holder.tvnama.setText(listData[position].getMahasiswa().getNim()+" - "+listData[position].getMahasiswa().getNama());
        holder.tvtanggalkumpul.setText(listData[position].getTanggal_pengumpulan());
        holder.tvKeterangan.setText("Keterangan : "+listData[position].getKeterangan());
    }

    @Override
    public int getItemCount() {
        return listData.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvnama, tvtanggalkumpul, tvKeterangan;

        public ViewHolder(View v) {
            super(v);
            context = v.getContext();

            tvnama = (TextView) v.findViewById(R.id.tvnama);
            tvtanggalkumpul = (TextView) v.findViewById(R.id.tvtanggalkumpul);
            tvKeterangan = (TextView) v.findViewById(R.id.tv_keterangan);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = listData[getAdapterPosition()].getFile();
                    if (!url.equals("-")){
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        context.startActivity(i);
                    }
                    else{
                        Toast.makeText(context, "Tidak ada file yang bisa diunduh", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

}
