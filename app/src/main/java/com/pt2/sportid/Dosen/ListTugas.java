package com.pt2.sportid.Dosen;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pt2.sportid.Model.EditProfil.EditProfilModel;
import com.pt2.sportid.Model.ListTugas.Data;
import com.pt2.sportid.Model.ListTugas.ListTugasModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;
import com.pt2.sportid.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListTugas extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ListTugasAdapter adapter;
    private Data[] dataList;
    TextView datakosong;
    static ApiService mApiService;
    private SharedPreferences sharedPref;
    public static Activity ListTugasAct;
    FloatingActionButton fab;
    public static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tugas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);
        recyclerView = (RecyclerView) findViewById(R.id.rvlistTugas);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ListTugasAct = this;
        datakosong = (TextView) findViewById(R.id.tv_datakosong);


        //shared preferences
        sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        getData();


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ListTugas.this,FormTambahTugas.class);
                startActivity(i);
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                hide(dy);
            }
        });
    }

    private void hide(int dy) {
        if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
            fab.hide();
        } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
            fab.show();
        }
    }

    public static void hapus(String id_tugas,String token){
        progressDialog.show();
        mApiService.destroyTugas(token,"1",id_tugas).enqueue(new Callback<EditProfilModel>() {
            @Override
            public void onResponse(Call<EditProfilModel> call, Response<EditProfilModel> response) {
                if(response.isSuccessful() && response.body().getStatus().equals("200")){
                    Toast.makeText(ListTugasAct.getApplicationContext(), "Tugas berhasil dihapus!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListTugasAct,ListTugas.class);
                    ListTugasAct.startActivity(i);
                    ListTugasAct.finish();
                }
                else{
                    Toast.makeText(ListTugasAct.getApplicationContext(), "Tugas gagal dihapus!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<EditProfilModel> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    progressDialog.hide();
                    Toast.makeText(ListTugasAct.getApplicationContext(), "Gagal memuat data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                    Log.wtf("sportsid", t.getMessage());
                } else {
                    progressDialog.hide();
                    Log.wtf("sportsid", t.getMessage());
                    Toast.makeText(ListTugasAct.getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void getData() {
        //nyokot token na ti shared preference sharedPref.getString(getString(R.string.token_pref),"0")
        progressDialog.show();
        mApiService.getListTugas(sharedPref.getString(getString(R.string.token_pref),"0")).enqueue(new Callback<ListTugasModel>() {
            @Override
            public void onResponse(Call<ListTugasModel> call, Response<ListTugasModel> response) {
                List<String> datalist = new ArrayList<String>();
                if(response.body().getData() != null) {
                    Data[] listTugas =  response.body().getData();
                    dataList = listTugas;
                    adapter = new ListTugasAdapter(getApplicationContext(),dataList);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setAdapter(new ListTugasAdapter(getApplicationContext(), dataList));
                }
                progressDialog.dismiss();

                if(adapter.getItemCount() == 0){
                    datakosong.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }


            @Override
            public void onFailure(Call<ListTugasModel> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    progressDialog.hide();
                    Toast.makeText(getApplicationContext(), "Gagal memuat data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                    Log.wtf("sportsid", t.getMessage());
                    finish();
                } else {
                    progressDialog.hide();
                    Log.wtf("sportsid", t.getMessage());
                    Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            ;
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    //back button onclick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}