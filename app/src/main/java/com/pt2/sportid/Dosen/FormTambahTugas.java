package com.pt2.sportid.Dosen;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.pt2.sportid.MainActivity;
import com.pt2.sportid.Model.Jurusan.JurusanModel;
import com.pt2.sportid.Model.Kelas.KelasModel;
import com.pt2.sportid.Model.Logout.LogoutModel;
import com.pt2.sportid.Model.TugasAdd;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;
import com.pt2.sportid.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormTambahTugas extends AppCompatActivity {

    ApiService mApiService;
    com.pt2.sportid.Model.Kelas.Data[] kelas;
    com.pt2.sportid.Model.Jurusan.Data[] jurusan;
    EditText etjudul,etKeterangan,etDeadline;
    Button btnTambah;
    Calendar myCalendar;
    Spinner spkelas,spjurusan;
    String id_kelas,id_jurusan;
    ProgressDialog progressDialog;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_tambah_tugas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        spkelas = (Spinner) findViewById(R.id.spkelas);
        btnTambah = (Button) findViewById(R.id.btn_submit);
        spjurusan= (Spinner) findViewById(R.id.spjurusan);
        etjudul = (EditText) findViewById(R.id.et_judul);
        etKeterangan = (EditText) findViewById(R.id.et_keterangan);
        etDeadline = (EditText) findViewById(R.id.et_deadline);


        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitTugas();
            }
        });

        setSpinnerJurusan();


        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        etDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(FormTambahTugas.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etDeadline.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void setSpinnerKelas(String id_jurusan) {
        progressDialog.show();
        mApiService.getListKelas("7878989sembilan#", id_jurusan).enqueue(new Callback<KelasModel>() {
            @Override
            public void onResponse(Call<KelasModel> call, Response<KelasModel> response) {
                kelas = response.body().getData();
                List<String> namaKelas = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < kelas.length; i++) {
                    namaKelas.add(kelas[i].getKelas());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaKelas);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spkelas.setAdapter(dataAdapter);
                spkelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_kelas = kelas[i].getId();
                        Log.wtf("id_kelas", id_kelas);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<KelasModel> call, Throwable t) {
                Log.wtf("spinner kelas", t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    void setSpinnerJurusan() {

        mApiService.getListJurusan("7878989sembilan#").enqueue(new Callback<JurusanModel>() {
            @Override
            public void onResponse(Call<JurusanModel> call, Response<JurusanModel> response) {
                jurusan = response.body().getData();
                List<String> namaJurusan = new ArrayList<String>();
                int i = 0;
                for (i = 0; i < jurusan.length; i++) {
                    namaJurusan.add(jurusan[i].getJurusan());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, namaJurusan);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spjurusan.setAdapter(dataAdapter);
                spjurusan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        id_jurusan = jurusan[i].getId();
                        setSpinnerKelas(id_jurusan);
                        Log.wtf("id_jurusan", id_jurusan);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }

            @Override
            public void onFailure(Call<JurusanModel> call, Throwable t) {
                Log.wtf("spinner jurusan", t.getMessage());
            }
        });
    }

    void submitTugas(){
        final String judul = etjudul.getText().toString();
        final String keterangan = etKeterangan.getText().toString();
        final String deadline = etDeadline.getText().toString();
        if (TextUtils.isEmpty(judul) ||
                TextUtils.isEmpty(keterangan) ||
                TextUtils.isEmpty(deadline) ||
                TextUtils.isEmpty(id_kelas) ||
                TextUtils.isEmpty(id_jurusan)){
            Toast.makeText(getApplicationContext(), "Mohon lengkapi kolom yang ada", Toast.LENGTH_SHORT).show();
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Konfirmasi");
            builder.setMessage("Apakah anda yakin dengan data ini?");
            builder.setPositiveButton("Ya",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            submit(judul, keterangan, deadline);
                        }
                    });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
    void submit(String judul , String keterangan, String deadline){
        progressDialog.show();
        mApiService.tugasAdd(judul,
                keterangan,
                sharedPref.getString(getString(R.string.id_dosen_pref),"0"),
                id_kelas,
                "1",
                sharedPref.getString(getString(R.string.token_pref),"0"),
                deadline).enqueue(new Callback<TugasAdd>() {
            @Override
            public void onResponse(Call<TugasAdd> call, Response<TugasAdd> response) {
                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equals("200")) {
                        Toast.makeText(getApplicationContext(), "Sukses menambahkan!", Toast.LENGTH_SHORT).show();
                        ListTugas.ListTugasAct.finish();
                        Intent i = new Intent(FormTambahTugas.this,ListTugas.class);
                        startActivity(i);
                        finish();
                    }

                } else {
                    progressDialog.hide();
                    Log.wtf("haseum",response.raw().toString());
                    Toast.makeText(getApplicationContext(), "Kesalahan!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<TugasAdd> call, Throwable t) {

            }
        });
    }

}
