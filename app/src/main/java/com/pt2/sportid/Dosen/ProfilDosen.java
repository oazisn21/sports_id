package com.pt2.sportid.Dosen;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pt2.sportid.Model.Profile.Data;
import com.pt2.sportid.Model.Profile.ProfileModel;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.NoConnectivityException;
import com.pt2.sportid.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilDosen extends AppCompatActivity {

    private ApiService mApiService;
     ImageView ivFotoProfil;
    EditText etKodeDosen, etNama, etUsername, etGender;
     ProgressDialog progressDialog;
     SharedPreferences sharedPref;
     int mode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_dosen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //initComponents
        mApiService = ApiUtils.getAPIService(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);
        etKodeDosen = (EditText) findViewById(R.id.et_kode_dosen);
        etNama = (EditText) findViewById(R.id.et_nama);
        etUsername = (EditText) findViewById(R.id.et_username);
        etGender = (EditText) findViewById(R.id.et_gender);
        ivFotoProfil = (ImageView) findViewById(R.id.iv_upload);

        etKodeDosen.setFocusable(false);
        etNama.setFocusable(false);
        etUsername.setFocusable(false);
        etGender.setFocusable(false);
        ivFotoProfil.setFocusable(false);

        //shared preferences
        sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

    }

    public void showData(final int mode){
        progressDialog.show();
        mApiService.getMyProfile(sharedPref.getString(getString(R.string.token_pref),"0")).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                if(response.isSuccessful()){

                    Glide.with(getApplicationContext())
                            .load(response.body().getData().getFoto())
                            .into(ivFotoProfil);
                    etKodeDosen.setText(response.body().getData().getKode_dosen());
                    etNama.setText(response.body().getData().getNama());
                    etUsername.setText(response.body().getData().getUser().getUsername());
                    if(response.body().getData().getJk().equals("L")){
                        etGender.setText("Laki-Laki");
                    }
                    else{
                        etGender.setText("Perempuan");
                    }
                    if(mode == 2){
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.id_dosen_pref), response.body().getData().getId());
                        editor.putString(getString(R.string.username_pref), response.body().getData().getUser().getUsername());
                        editor.putString(getString(R.string.kd_dosen_pref), response.body().getData().getKode_dosen());
                        editor.putString(getString(R.string.gender), response.body().getData().getJk());
                        editor.putString(getString(R.string.nama_pref), response.body().getData().getNama());
                        editor.commit();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    progressDialog.hide();
                    Toast.makeText(getApplicationContext(), "Gagal memuat data! Periksa internet anda", Toast.LENGTH_SHORT).show();
                    Log.wtf("sportsid", t.getMessage());
                    finish();
                } else {
                    progressDialog.hide();
                    Log.wtf("sportsid", t.getMessage());
                    Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        showData(mode);
        mode = 2;
    }

    //back button onclick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_edit :
                Intent i = new Intent(this,UbahProfil.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profil, menu);
//        return super.onCreateOptionsMenu(menu);
        return true;
    }
}
