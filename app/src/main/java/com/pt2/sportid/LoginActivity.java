package com.pt2.sportid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pt2.sportid.Model.Login.LoginModel;
import com.pt2.sportid.Model.Profile.ProfileModel;
import com.pt2.sportid.Network.ApiUtils;
import com.pt2.sportid.Network.ApiService;
import com.pt2.sportid.Network.NoConnectivityException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText etUsername, etPassword;
    Button btnLogin;
    ProgressDialog progressDialog;
    ApiService mApiService;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Masuk");
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setCancelable(false);

        //add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mApiService = ApiUtils.getAPIService(this);

        //initComponents
        etUsername = (EditText) findViewById(R.id.et_username);
        etPassword = (EditText) findViewById(R.id.et_password);
        btnLogin = (Button) findViewById(R.id.btn_login);

        //setOnclickListener
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = etUsername.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
//                Log.wtf("e","ok1");
                if(!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
                    progressDialog.show();
                    login(username,password);
//                    Log.wtf("e","ok2");

                }
                else{
//                    Log.wtf("e","ok3");
                    Snackbar snackbar = Snackbar.make(getCurrentFocus(),"Username dan password tidak boleh kosong!", Snackbar.LENGTH_SHORT);
                    snackbar.show();
//                    Toast.makeText(getApplicationContext(), "Username dan Password kosong!",Toast.LENGTH_SHORT);
                }
            }
        });

        //shared preferences
        sharedPref = LoginActivity.this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    void login(String username, String password){

        mApiService.login(username,password,"gatau").enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if(response.code()==200 && response.body().getStatus().equals("200")){
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(getString(R.string.token_pref), response.body().getData().getToken());
                    editor.commit();

                    //get token
                    String token = sharedPref.getString(getString(R.string.token_pref),"0");
                    Log.wtf("token", token);

                    //getmyprofile
                    mApiService.getMyProfile(token).enqueue(new Callback<ProfileModel>() {
                        @Override
                        public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                            if(response.body().getStatus().equals("200")){
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString(getString(R.string.id_role_pref), response.body().getData().getUser().getId_role());
                                editor.putString(getString(R.string.username_pref), response.body().getData().getUser().getUsername());
                                editor.putString(getString(R.string.nama_pref), response.body().getData().getNama());
                                editor.commit();

                                if (response.body().getData().getUser().getId_role().equals("1")){ //role guru
                                    Intent i = new Intent(getApplicationContext(),DashboardDosen.class);
                                    editor = sharedPref.edit();
                                    editor.putString(getString(R.string.id_dosen_pref), response.body().getData().getId());
                                    editor.commit();
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); //kill old activity
                                    startActivity(i);
                                    finish();
                                }
                                else if (response.body().getData().getUser().getId_role().equals("2")){ //role siswa
                                    Intent i = new Intent(getApplicationContext(),DashboardMahasiswa.class);
                                    editor = sharedPref.edit();
                                    editor.putString(getString(R.string.id_dosen_pref), response.body().getData().getId_dosen());
                                    editor.putString(getString(R.string.id_jurusan_pref), response.body().getData().getId_jurusan());
                                    editor.commit();
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); //kill old activity
                                    startActivity(i);
                                    finish();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ProfileModel> call, Throwable t) {
                            if(t instanceof NoConnectivityException) {
                                progressDialog.hide();
                                Snackbar snackbar = Snackbar.make(getCurrentFocus(),"Login gagal! Cek internet anda.", Snackbar.LENGTH_SHORT);
                                snackbar.show();
                                Log.wtf("sportsid", t.getMessage());
                            }
                            else {
                                progressDialog.hide();
                                Log.wtf("sportsid", t.getMessage());
                                Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    progressDialog.hide();
                    Snackbar snackbar = Snackbar.make(getCurrentFocus(),"Nama Penggunama atau Kata Sandi salah!", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    Log.wtf("sportsid", response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                if(t instanceof NoConnectivityException) {
                    progressDialog.hide();
                    Snackbar snackbar = Snackbar.make(getCurrentFocus(),"Login gagal! Cek internet anda.", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    Log.wtf("sportsid", t.getMessage());
                }
                else {
                    progressDialog.hide();
                    Log.wtf("sportsid", t.getMessage());
                    Toast.makeText(getApplicationContext(), "Unknown Error!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //back button onclick
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
