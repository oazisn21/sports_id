package com.pt2.sportid.Network;

import android.content.Context;

public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "https://sportsid-api.herokuapp.com/";

    public static ApiService getAPIService(Context context) {

        return RetrofitClient.getClient(context,BASE_URL).create(ApiService.class);
    }
}
