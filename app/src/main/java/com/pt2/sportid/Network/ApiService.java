package com.pt2.sportid.Network;

import com.pt2.sportid.Model.CekPengerjaanTugas.CekPengerjaanTugasModel;
import com.pt2.sportid.Model.DetilTugas.DetilTugasModel;
import com.pt2.sportid.Model.Dosen.DosenModel;
import com.pt2.sportid.Model.EditProfil.EditProfilModel;
import com.pt2.sportid.Model.Jurusan.JurusanModel;
import com.pt2.sportid.Model.Kelas.KelasModel;
import com.pt2.sportid.Model.KumpulTugasModel.KumpulTugasModel;
import com.pt2.sportid.Model.LihatTugas.LihatTugasModel;
import com.pt2.sportid.Model.ListTugas.ListTugasModel;
import com.pt2.sportid.Model.ListTugasMahasiswa.ListMahasiswaModel;
import com.pt2.sportid.Model.Login.LoginModel;
import com.pt2.sportid.Model.Logout.LogoutModel;
import com.pt2.sportid.Model.NotificationCounterTugas.NotificationCounterTugasModel;
import com.pt2.sportid.Model.Profile.ProfileModel;
import com.pt2.sportid.Model.Register.RegisterModel;
import com.pt2.sportid.Model.TugasAdd;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface ApiService {
    @POST("login")
    @FormUrlEncoded
    Call<LoginModel> login(
            @Field("username") String username,
            @Field("password") String password,
            @Field("device_id") String device_id);

    @POST("tugas/add")
    @FormUrlEncoded
    Call<TugasAdd> tugasAdd(
            @Field("judul") String judul,
            @Field("keterangan") String keterangan,
            @Field("id_dosen") String id_dosen,
            @Field("id_kelas") String id_kelas,
            @Field("id_role") String id_role,
            @Field("token") String token,
            @Field("deadline") String deadline);

    @POST("get/profile")
    @FormUrlEncoded
    Call<ProfileModel> getMyProfile(
            @Field("token") String token);

    @POST("notif/tugas")
    @FormUrlEncoded
    Call<NotificationCounterTugasModel> countNewTugas(
            @Field("token") String token);

    @POST("notif/pengumpulan")
    @FormUrlEncoded
    Call<NotificationCounterTugasModel> countNewPengumpulan(
            @Field("token") String token);

    @POST("tugas/detil")
    @FormUrlEncoded
    Call<DetilTugasModel> getDetilTugas(
            @Field("token") String token,
            @Field("id_tugas") String id_tugas);

    @POST("logout")
    @FormUrlEncoded
    Call<LogoutModel> logout(
            @Field("token") String token);

    @POST("get/kelas")
    @FormUrlEncoded
    Call<KelasModel> getListKelas(
            @Field("password") String password,
            @Field("id_jurusan") String id_jurusan
            );

    @POST("get/jurusan")
    @FormUrlEncoded
    Call<JurusanModel> getListJurusan(
            @Field("password") String password
    );


    @POST("get/dosen")
    @FormUrlEncoded
    Call<DosenModel> getListDosen(
            @Field("password") String password
    );

    @POST("tugas")
    @FormUrlEncoded
    Call<ListTugasModel> getListTugas(
            @Field("token") String token
    );

    @POST("tugas")
    @FormUrlEncoded
    Call<ListMahasiswaModel> getListTugasmhs(
            @Field("token") String token
    );

    @POST("tugas/delete")
    @FormUrlEncoded
    Call<EditProfilModel> destroyTugas(
            @Field("token") String token,
            @Field("id_role") String id_role,
            @Field("id") String id_tugas
    );

    @POST("tugas/cek")
    @FormUrlEncoded
    Call<CekPengerjaanTugasModel> cekPengerjaanTugas(
            @Field("token") String token,
            @Field("id_tugas") String id_tugas
    );

    @POST("tugas/cek/pengerjaan")
    @FormUrlEncoded
    Call<LihatTugasModel> lihatTugas(
            @Field("token") String token,
            @Field("id_tugas") String id_tugas
    );


    @POST("tugas/edit")
    @FormUrlEncoded
    Call<EditProfilModel> editTugas(
            @Field("token") String token,
            @Field("judul") String judul,
            @Field("keterangan") String keterangan,
            @Field("id_dosen") String id_dosen,
            @Field("id_kelas") String id_kelas,
            @Field("deadline") String deadline,
            @Field("id") String id_tugas
    );

    @Multipart
    @POST("register")
    Call<RegisterModel> regis(
            @Part("id_role")RequestBody id_role,
            @Part("username") RequestBody username,
            @Part("password") RequestBody password,
            @Part("nim") RequestBody nim,
            @Part("nama") RequestBody nama,
            @Part("id_jurusan") RequestBody id_jurusan,
            @Part("id_dosen") RequestBody id_dosen,
            @Part("id_kelas") RequestBody id_kelas,
            @Part("jk") RequestBody jk,
            @Part MultipartBody.Part image,
            @Part("device_id") RequestBody device_id
            );

    @Multipart
    @POST("tugas/kumpulkan")
    Call<KumpulTugasModel> kumpulkan(
            @Part("token")RequestBody token,
            @Part("id_tugas")RequestBody id_tugas,
            @Part("tanggal_pengumpulan") RequestBody tanggal_pengumpulan,
            @Part("keterangan") RequestBody keterangan,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST("tugas/kumpulkan")
    Call<KumpulTugasModel> kumpulkanTanpaFile(
            @Part("token")RequestBody token,
            @Part("id_tugas")RequestBody id_tugas,
            @Part("tanggal_pengumpulan") RequestBody tanggal_pengumpulan,
            @Part("keterangan") RequestBody keterangan
    );

    @Multipart
    @POST("register")
    Call<RegisterModel> regisdosen(
            @Part("id_role")RequestBody id_role,
            @Part("username") RequestBody username,
            @Part("password") RequestBody password,
            @Part("nama") RequestBody nama,
            @Part("jk") RequestBody jk,
            @Part MultipartBody.Part image,
            @Part("device_id") RequestBody device_id,
            @Part("kode_dosen") RequestBody kode_dosen
            );

    @Multipart
    @POST("profile/edit")
    Call<EditProfilModel> editProfilDosen(
            @Part("token")RequestBody token,
            @Part("id_role")RequestBody id_role,
            @Part("username") RequestBody username,
            @Part("password") RequestBody password,
            @Part("nama") RequestBody nama,
            @Part("jk") RequestBody jk,
            @Part MultipartBody.Part image,
            @Part("kode_dosen") RequestBody kode_dosen
    );

    @Multipart
    @POST("profile/edit")
    Call<EditProfilModel> editProfilMahasiswa(
            @Part("token")RequestBody token,
            @Part("id_role")RequestBody id_role,
            @Part("username") RequestBody username,
            @Part("password") RequestBody password,
            @Part("nim") RequestBody nim,
            @Part("nama") RequestBody nama,
            @Part("jk") RequestBody jk,
            @Part MultipartBody.Part image,
            @Part("id_jurusan") RequestBody id_jurusan,
            @Part("id_dosen") RequestBody id_dosen,
            @Part("id_kelas") RequestBody id_kelas
    );

}
